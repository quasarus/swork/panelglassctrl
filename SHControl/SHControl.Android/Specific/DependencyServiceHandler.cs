﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.BLE.Abstractions.Contracts;

using SHControl.DependencyServices;
using SHControl.Droid.Specific;


[assembly: Xamarin.Forms.Dependency(typeof(DependencyServiceHandler))]
namespace SHControl.Droid.Specific
{
    /// <summary>
    /// Реализация операций, специфичных для Android OS (служба зависимостей)
    /// </summary>
    public class DependencyServiceHandler : IDepServices
    {
        /// <summary>
        /// Включение Bluetooth
        /// </summary>
        /// <returns></returns>
        public Task EnableBluetooth()
        {
            return Task.Factory.StartNew(() =>
            {
                //менеджер Bluetooth
                BluetoothManager btManager = (BluetoothManager)Application.Context.GetSystemService(Context.BluetoothService);
                //включение Bluetooth
                btManager.Adapter.Enable();

            });

        }

        [Obsolete]
        public void ExitApp()
        {
            //System.Environment.Exit(0);
            //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            var activity = (Activity)Xamarin.Forms.Forms.Context;
            activity.FinishAffinity();
        }

        public string GetVersion()
        {
            var context = Android.App.Application.Context;
            return context.PackageManager.GetPackageInfo(context.PackageName, PackageInfoFlags.MetaData).VersionName;
            //var BuildNumber = context.PackageManager.GetPackageInfo(context.PackageName, PackageInfoFlags.MetaData).VersionCode.ToString();
        }

        /// <summary>
        /// Является ли устройство BLE
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public bool IsBleDevice(IDevice device)
        {
            try
            {
                if (device.GetType() != typeof(Plugin.BLE.Android.Device))
                {
                    return false;
                }
                var bleDev = (Plugin.BLE.Android.Device)device;
                return bleDev.BluetoothDevice.Type == BluetoothDeviceType.Le ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}