﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SHControl.Droid.Renderers;
using SHControl.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustListView), typeof(AndroidLw))]
namespace SHControl.Droid.Renderers
{
    /// <summary>
    /// Прорисовщик ListView
    /// </summary>
    [Preserve(AllMembers = true)]
    
    public class AndroidLw : ListViewRenderer
    {       

        public AndroidLw(Context context) : base(context)
        {
            
        }

        protected override void OnAttachedToWindow()
        {            
            base.OnAttachedToWindow();
        }



        protected override void OnDetachedFromWindow()
        {            
            base.OnDetachedFromWindow();                              
        }        

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);            
        }        

        protected override void Dispose(bool disposing)
        {            
            base.Dispose(disposing);
        }

        

    }
}