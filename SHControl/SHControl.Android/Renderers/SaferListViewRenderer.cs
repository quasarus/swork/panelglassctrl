﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using SHControl.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Android.Widget.AbsListView;

[assembly: ExportRenderer(typeof(Xamarin.Forms.ListView), typeof(SaferListViewRenderer))]
namespace SHControl.Droid.Renderers
{
    public class SaferListViewRenderer : ListViewRenderer
    {

        public SaferListViewRenderer(Context context) : base (context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                this.Control.Recycler -= ClearViewFocus;
                this.Control.Recycler += ClearViewFocus;
            }
        }

        private void ClearViewFocus(object sender, AbsListView.RecyclerEventArgs args)
        {
            if (args.View.HasFocus)
            {
                args.View.ClearFocus();

                var manager = (InputMethodManager)args.View.Context.GetSystemService(Context.InputMethodService);

                if (manager != null)
                    manager.HideSoftInputFromWindow(args.View.WindowToken, 0);
            }
        }
    }
}

    