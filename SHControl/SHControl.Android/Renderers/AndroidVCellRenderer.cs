﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SHControl.Droid.Renderers;
using SHControl.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustViewCell), typeof(NativeAndroidCellRenderer))]
namespace SHControl.Droid.Renderers
{
    /// <summary>
    /// Прорисовщик ячейки ListView
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class NativeAndroidCellRenderer : ViewCellRenderer
    {
        protected override Android.Views.View GetCellCore(Cell item, Android.Views.View convertView, ViewGroup parent, Context context)
        {
            return base.GetCellCore(item, convertView, parent, context);
        }
        
    }
}