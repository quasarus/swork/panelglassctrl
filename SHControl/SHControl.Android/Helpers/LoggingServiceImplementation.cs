﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SHControl.Droid.Helpers
{
    /// <summary>
    /// Реализация логгера на Android
    /// </summary>
    [Preserve(AllMembers = true)]
    public static class LoggingServiceImplementation
    {
        private const long LogSizeThrh = 500000;        //размер файла лога, при превышении которого он очищается

        public static string LogFolder = Android.OS.Environment.ExternalStorageDirectory + "/Log";

        public static string LogFile => LogFolder + "/Log.txt";
        /// <summary>
        /// Логгировать исключение
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ex"></param>
        public static void Log(string userId, Exception ex)
        {
            //Если папка отсутствует - создать
            if (!Directory.Exists(LogFolder))
            {
                Directory.CreateDirectory(LogFolder);
            }
            FileInfo fi = new FileInfo(LogFile);

            //проверить размер файла, если больше порогового - очистить
            if (fi.Length > LogSizeThrh)
            {
                File.WriteAllText(LogFile, "");                
            }

            //записать инфо об исключении
            using (StreamWriter sw = File.AppendText(LogFile))
            {
                sw.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm}\t{userId}\t{ex.Message}\t{ex.InnerException}\t{ex.StackTrace}");
                sw.WriteLine(string.Empty);
            }
        }

        /// <summary>
        /// Логгировать сообщение
        /// </summary>
        /// <param name="mess"></param>
        public static void LogMess(string mess)
        {
            if (!Directory.Exists(LogFolder))
            {
                Directory.CreateDirectory(LogFolder);
            }

            using (StreamWriter sw = File.AppendText(LogFile))
            {
                sw.WriteLine($"{DateTime.Now:dd/MM/yyyy HH:mm}\t{mess}");
                sw.WriteLine(string.Empty);
            }
        }
    }
}