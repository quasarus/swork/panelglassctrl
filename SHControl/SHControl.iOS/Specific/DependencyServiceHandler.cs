﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using Plugin.BLE.Abstractions.Contracts;
using SHControl.DependencyServices;
using UIKit;

namespace SHControl.iOS.Specific
{
    public class DependencyServiceHandler : IDepServices
    {
        public Task EnableBluetooth()
        {
            throw new NotImplementedException();
        }

        public void ExitApp()
        {
            Thread.CurrentThread.Abort();
        }

        public string GetVersion()
        {
            return NSBundle.MainBundle.InfoDictionary.ValueForKey(new NSString("CFBundleShortVersionString")).ToString();
            //var BuildNumber = NSBundle.MainBundle.InfoDictionary.ValueForKey(new NSString("CFBundleVersion")).ToString();
        }

        public bool IsBleDevice(IDevice device)
        {
            throw new NotImplementedException();
        }
    }
}