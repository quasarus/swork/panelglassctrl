﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.Behavior
{
    /// <summary>
    /// Базовый класс для реализации поведения по событию
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class BehaviorBase<T> : Behavior<T> where T : BindableObject
    {
        public T AssociatedObject { get; private set; }

        /// <summary>
        /// При подсоединении к свойству зависимости
        /// </summary>
        /// <param name="bindable"></param>
        protected override void OnAttachedTo(T bindable)
        {
            base.OnAttachedTo(bindable);
            AssociatedObject = bindable;

            if (bindable.BindingContext != null)
            {
                BindingContext = bindable.BindingContext;
            }

            bindable.BindingContextChanged += OnBindingContextChanged;
        }

        /// <summary>
        /// При отсоединении от свойства зависимости
        /// </summary>
        /// <param name="bindable"></param>
        protected override void OnDetachingFrom(T bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.BindingContextChanged -= OnBindingContextChanged;
            AssociatedObject = null;
        }


        /// <summary>
        /// При изменении контекста привязки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnBindingContextChanged(object sender, EventArgs e)
        {
            OnBindingContextChanged();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            BindingContext = AssociatedObject.BindingContext;
        }
    }
}
