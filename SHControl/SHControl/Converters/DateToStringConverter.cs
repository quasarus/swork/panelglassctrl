﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace SHControl.Converters
{
    /// <summary>
    /// Конвертер даты/времени в форматированную строку
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class DateToStringConverter : IValueConverter
    {
        /// <summary>
        /// При конвертации
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            return $"{string.Format("{0:00}",dt.Day)}/{string.Format("{0:00}", dt.Month)}/{string.Format("{0:00}", dt.Year)}" +
                $" {string.Format("{0:00}", dt.Hour)}:{string.Format("{0:00}", dt.Minute)}:{string.Format("{0:00}", dt.Second)}";
        }

        /// <summary>
        /// При обратной конвертации
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
