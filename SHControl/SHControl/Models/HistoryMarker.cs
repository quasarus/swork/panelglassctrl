﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace SHControl.Models
{
    /// <summary>
    /// дополнительные данные признаков
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class HistoryMarkerValue
    {
        /// <summary>
        /// Длина данных
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public uint Val { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        public HistoryMarkerValue(int length,string descr)
        {
            Length = length;            
            Description = descr;
        }
    }

    /// <summary>
    /// Данные признаков истории
    /// </summary>
    public class HistoryMarker
    {
        /// <summary>
        /// Строка признака
        /// </summary>
        public string Marker { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Дополнительные данные признаков
        /// </summary>
        public List<HistoryMarkerValue> Values { get; set; }

        public HistoryMarker()
        {
            Values = new List<HistoryMarkerValue>();
        }

        /// <summary>
        /// Получение дополнительных данных признака
        /// </summary>
        /// <param name="lastPart"></param>
        public void GetMarkerValuesFromString(string lastPart)
        {
            //проверка наличия строки доп. параметров
            if (string.IsNullOrEmpty(lastPart))
            {
                return;
            }

            //бросить исключение, если длина доп. данных не равна заданной
            if (lastPart.Length != Values.Sum(f=>f.Length))
            {
                throw new Exception("Неверная длина дополнительных данных признака.");
            }

            int offset = 0;                 //смещение в строке доп. данных
            foreach (var mrk in Values)
            {
                try
                {
                    //парсинг подстроки, содержащей значение
                    mrk.Val = uint.Parse(lastPart.Substring(offset, mrk.Length));
                    //инкремент смещения на длину подстроки
                    offset += mrk.Length;                    
                }
                catch (Exception)
                {
                    throw new Exception("Не удалось получить дополнительные данные признака.");
                }

            }
        }

        /// <summary>
        /// Получение описания доп. параметров
        /// </summary>
        /// <returns></returns>
        public string GetValuesInfo()
        {
            if (Values.Count == 0)
            {
                return "";
            }
            else
            {
                string descr = "";
                //инфо о каждом параметре в столбик, для отображения в ListView
                foreach (var val in Values)
                {
                    descr += $"{val.Description} = {val.Val.ToString()}\r\n";
                }
                return descr.TrimEnd(new char[] {'\r','\n' });
            }

        }



    }


}
