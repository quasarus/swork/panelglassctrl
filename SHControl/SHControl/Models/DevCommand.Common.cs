﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using SHControl.Helpers;
using SHControl.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms.Internals;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;

namespace SHControl.Models
{
    //*************************************************************
    //Общая реализация команд взаимодействия с панелями
    //*************************************************************

    /// <summary>
    /// Общая реализация команды
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public abstract class DevCommand : ObservableObject,IDisposable
    {
        
        public Func<string, string> CommandAnswerCallback { get; set; }
        /// <summary>
        /// Описание команды
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Базовая строка команды
        /// </summary>
                
        public string ComString { get; set; }

        /// <summary>
        /// Текст кнопки выполнения
        /// </summary>
        public string ExecText { get; set; }
               
        /// <summary>
        /// Команда запуска на выполнение
        /// </summary>
        public ICommand ExecCommand { get; set; }
        /// <summary>
        /// Список выбранных панелей
        /// </summary>

        protected List<DevPanel> SelectedPanels { get; set; }
        /// <summary>
        /// Смещается ли Описание команды в списке в GUI (т.е является вложенной в более высокий уровень) 
        /// </summary>

        public bool IsLeftOffset { get; set; }

        /// <summary>
        /// Отправка команды не содержащей пакет AT-команд для модуля Bluetooth, который уже подключен
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        protected async Task<string> SendNonAtCommandForConnectedDevice(string command)
        {
            
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            return await btConnector.SendCommandAsync(command,false);             
        }

        /// <summary>
        /// Отправка команды не содержащей пакет AT-команд для модуля Bluetooth, который еще не подключен
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        protected async Task<string> SendNonAtCommandForDevice(DevPanel panel, string command)
        {
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            await btConnector.ConnectToDeviceAsync(panel);
            string answer = await btConnector.SendCommandAsync(command, false);
            await btConnector.DisconnectDeviceAsync();
            return answer;
        }

        /// <summary>
        /// Отправка команды,содержащей пакет AT-команд для модуля Bluetooth, который уже подключен
        /// </summary>
        /// <param name="command"></param>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected async Task SendCommandAndPacketForConnectedDevice(string command,List<string> packet)
        {
            if (!string.IsNullOrEmpty(command))
            {
                await SendNonAtCommandForConnectedDevice(command);
            }            
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            foreach (var packetPart in packet)
            {
                await btConnector.SendCommandAsync(packetPart, false);
            }
            await btConnector.SendCommandAsync(";", true);
            //await btConnector.ConnectToDeviceAsync(btConnector.GetConnectedPanel());
        }

        /// <summary>
        /// Отправка команды, содержащей пакет AT-команд для модуля Bluetooth, который еще не подключен
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="command"></param>
        /// <param name="packet"></param>
        /// <returns></returns>
        protected async Task SendCommandAndPacketForDevice(DevPanel panel,string command, List<string> packet)
        {
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            await btConnector.ConnectToDeviceAsync(panel);
            if (!string.IsNullOrEmpty(command))
            {
                await SendNonAtCommandForConnectedDevice(command);
            }           
            
            await SendCommandAndPacketForConnectedDevice(null, packet);
            
        }

        public virtual async Task<bool> ExecAdditionalCommand()
        {
            return true;
        }

        public DevCommand()
        {
            SelectedPanels = SimpleIoc.Default.GetInstance<IUserDataStore>().GetSelectedPanels();
        }



        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DevCommand() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion



    }

    /// <summary>
    /// Реализация команды, которая имеет вложенные команды
    /// </summary>

    [Android.Runtime.Preserve(AllMembers = true)]
    public class SimpleCommandSub : DevCommand
    {

        

        private List<DevCommand> _subCommands;
        /// <summary>
        /// Вложенные команды
        /// </summary>
        public List<DevCommand> SubCommands
        {
            get { return _subCommands; }
            set
            {
                Set(nameof(SubCommands), ref _subCommands, value);
            }
        }

        /// <summary>
        /// Команда по событию тапа по команде, имеющей вложенные команды
        /// </summary>
        public ICommand ClickCommand { get; set; }

        private bool _isSelected;
        /// <summary>
        /// Флаг того, что вложенные команды раскрыты
        /// </summary>
        [Android.Runtime.Preserve]        
        public bool IsComSelected
        {
            get { return _isSelected; }
            set
            {
                //если раскрыты дочерние элементы - выполнить для всех дополнительную команду
                if (value)
                {
                    ExecAdditionalCommandForChildren();                    
                }                
                Set(nameof(IsComSelected), ref _isSelected, value);
            }
        }

        /// <summary>
        /// Выполнить доп. команды для всех дочерних команд
        /// </summary>
        private async void ExecAdditionalCommandForChildren()
        {
            foreach (var command in SubCommands)
            {
                //если одна из команд выполнилась с ошибкой - прервать выполнение
                if (!await command.ExecAdditionalCommand())
                {
                    break;
                }                
            }
        }

        protected override void Dispose(bool disposing)
        {
            SubCommands.Clear();
            base.Dispose(disposing);
        }
                

        public SimpleCommandSub()
        {
            SubCommands = new List<DevCommand>();
            //по событию тапа по команде, имеющей вложенные команды инвертировать флаг раскрытия вложенных команд
            ClickCommand = new RelayCommand(() => { IsComSelected = !IsComSelected; });
            IsComSelected = false;
        }
    }

    

    /// <summary>
    /// Реализация команды, в которой используется ввод в Entry
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SimpleCommandTextbox : SimpleCommand
    {
        private string _text;
        /// <summary>
        /// Вводимый в Entry текст
        /// </summary>
        public string Text
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    CheckInput(value);                  //проверка ввода
                    Set(nameof(Text), ref _text, value);
                }
                
            }
        }

        private bool _isValid;
        /// <summary>
        /// Флаг того, что ввод верен
        /// </summary>
        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                Set(nameof(IsValid), ref _isValid, value);
            }
        }

        private bool _isNumbersOnly;
        /// <summary>
        /// Флаг, что вводимое в Entry значение - только цифры
        /// </summary>
        public bool IsNumbersOnly
        {
            get { return _isNumbersOnly; }
            set
            {
                Set(nameof(IsNumbersOnly), ref _isNumbersOnly, value);
            }
        }

        private string errText;
        /// <summary>
        /// Сообщение ошибки ввода
        /// </summary>
        public string ErrText
        {
            get { return errText; }
            set
            {
                //если сообщение ошибки ввода ненулевое, то установить флаг, иначе очистить
                IsValid = string.IsNullOrEmpty(value) ? true : false;
                Set(nameof(ErrText), ref errText, value);

            }
        }

        
        /// <summary>
        /// Максимальная длина вводимых в Entry символов
        /// </summary>
        public int MaxLength { get; set; }


        /// <summary>
        /// Общая проверка ввода в Entry
        /// </summary>
        /// <param name="value"></param>
        private void CheckInput(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                ErrText = "Введите значение";
                return;
            }
            //дополнительная проверка ввода  в Entry - реализуется потомками
            ErrText = GetAdditionalInputError(value);
        }

        /// <summary>
        /// Дополнительная проверка ввода  в Entry
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual string GetAdditionalInputError(string value)
        {
            //заглушка
            return "";
        }

        [Preserve]
        public SimpleCommandTextbox()
        {
            MaxLength = 12;
            Text = "";
        }
    }

    /// <summary>
    /// Реализация команды, в которой используется ввод в пикером
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SimpleCommandPicker : SimpleCommand
    {
        /// <summary>
        /// Список элементов пикера
        /// </summary>
        public ObservableCollection<PickerData> PickerItems { get; set; }
        

        private bool _isValid;
        /// <summary>
        /// Флаг того, что выбрано в пикере что-либо
        /// </summary>
        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                Set(nameof(IsValid), ref _isValid, value);
            }
        }

        /// <summary>
        /// Выьранный в пикере элемент
        /// </summary>
        private PickerData _selItem;
        public PickerData SelectedItem
        {
            get { return _selItem; }
            set
            {
                
                IsValid = value == null ? false : true;
                Set(nameof(SelectedItem), ref _selItem, value);
            }
        }        


    }

    /// <summary>
    /// Элементы пикера
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class PickerData
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Добавочная строка для не-АТ-команд
        /// </summary>
        public string AddString { get; set; }
        /// <summary>
        /// Добавочная строка для АТ-команд (пакетов)
        /// </summary>
        public string AddStringPacket { get; set; }
    }
}
