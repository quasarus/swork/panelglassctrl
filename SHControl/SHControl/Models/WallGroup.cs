﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using System.Collections;
using System.Runtime.CompilerServices;

namespace SHControl.Models
{
    /// <summary>
    /// Стена
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class WallGroup: ObservableObject
    {
        /// <summary>
        /// Имя стены
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Идентификатор стены
        /// </summary>

        public Guid Id { get; set; }
        
        /// <summary>
        /// Команда тапа по элементу стены
        /// </summary>
        public ICommand WallTapCommand { get; set; }

        /// <summary>
        /// Панели, входящие в стену
        /// </summary>
        public ObservableCollection<DevPanel> Panels { get; set; }

        /// <summary>
        /// Флаг того, что GUI элементы панелей раскрыты тапом по элементу стены
        /// </summary>
        private bool _isExpanded;        
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                Set(nameof(IsExpanded), ref _isExpanded, value);
            }
        }

        /// <summary>
        /// Флаг, что стена выбрана чекбоксом
        /// </summary>
        private bool _isSelectedByCheckBox;

        public bool IsSelectedByCheckBox
        {
            get { return _isSelectedByCheckBox; }
            set
            {
                Set(nameof(IsSelectedByCheckBox), ref _isSelectedByCheckBox, value);
                //установить или сбросить флаг выбора для всех панелей, входящих в стену
                foreach (var panel in Panels)
                {
                    panel.IsSelectedByCheckBox = value;
                }
                //отправить сообщение, что выбрана стена
                Messenger.Default.Send(new NotificationMessage<bool>(value, "IsSelectedByCheckBox"));
                
            }
        }

        /// <summary>
        /// Добавление новой панели
        /// </summary>
        /// <param name="newPanel"></param>
        public void AddPanel(DevPanel newPanel)
        {
            newPanel.Id = Guid.NewGuid();
            Panels.Add(newPanel);            
        }

        public WallGroup(Guid id)
        {
            Id = id;
            Panels = new ObservableCollection<DevPanel>();
            //при тапе по эл-ту стены, инвертировать флаг раскрытия элементов панелей
            WallTapCommand = new RelayCommand(()=> { IsExpanded = !IsExpanded; });            
        }

        
    }
}
