﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Models
{
    /// <summary>
    /// Данные истории
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class HistoryData : ObservableObject
    {
        private DateTime _date;
        /// <summary>
        /// Дата\время
        /// </summary>
        public DateTime Date
        {
            get { return _date; }
            set
            {
                Set(nameof(Date), ref _date, value);
            }
        }


        private string _actDescr;
        /// <summary>
        /// Описание действия
        /// </summary>
        public string ActionDescr
        {
            get { return _actDescr; }
            set
            {
                Set(nameof(ActionDescr), ref _actDescr, value);
            }
        }

        private string _addInfo;
        /// <summary>
        /// дополнительная информаация
        /// </summary>
        public string AdditionalInfo
        {
            get { return _addInfo; }
            set
            {
                Set(nameof(AdditionalInfo), ref _addInfo, value);
            }
        }
        /// <summary>
        /// Признак истории
        /// </summary>
        public string Marker { get; set; }

    }
        
}
