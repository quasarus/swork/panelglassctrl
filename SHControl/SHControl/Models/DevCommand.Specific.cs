﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using SHControl.Messages;
using SHControl.Services;
using SHControl.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using System.Globalization;
using SHControl.ViewModels;
using SHControl.Helpers;
using System.Windows.Input;

//*************************************************************
//Конкретная реализация команд взаимодействия с панелями, унаследованных от общей реализации команд
//*************************************************************

namespace SHControl.Models
{
    /// <summary>
    /// Простая команда на модуль Bluetooth
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SimpleCommand : DevCommand
    {
        

        public SimpleCommand()
        {
            //StoredParameter = StoredParameterType.None;
            ExecCommand = new RelayCommand(async ()=> { await OnExecCommand(); });
        }       
        
        /// <summary>
        ///По выполнению команды
        /// </summary>
        public async Task<bool> OnExecCommand()
        {
            //отправка уведомления о длительной операции
            Messenger.Default.Send(new BusyMessage("Отправка команды", true));

            try
            {
                await ExecuteCommand();
                await AfterSuccessCommandExecAsync();
                return true;
            }
            catch (Exception ex)
            {
                //разрыв сопряжения
                var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
                await btConnector.DisconnectDeviceAsync();

                //вывод уведомления об ошибке
                await App.Current.MainPage.DisplayAlert("Ошибка",
                                                    ex.Message, "Ok");

                //переход на страницу с списком стен
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
            }
            finally
            {
                //в любом случае отправить уведомление о завершении длительной операции
                Messenger.Default.Send(new BusyMessage("", false));
            }
            return false;
        }


        /// <summary>
        /// По завершению успешного выполнения команды
        /// </summary>
        /// <returns></returns>
        protected virtual async Task AfterSuccessCommandExecAsync()
        {
            //заглушка
        }

        /// <summary>
        /// Обработчик команды
        /// </summary>
        /// <returns></returns>
        protected virtual async Task ExecuteCommand()
        {

            string command = PrepareCommand(ComString);         //подготовка команды

            //если выбрана 1 панель, начать выполнение команд для одной панели
            if (SelectedPanels.Count == 1)
            {                
                string answer = await SendNonAtCommandForConnectedDevice(command);
                HandleAnswer(answer);
            }
            else
            {
                foreach (var panel in SelectedPanels)
                {
                    string answer = await SendNonAtCommandForDevice(panel, command);
                    HandleAnswer(answer);
                }
            }

        }

        
        /// <summary>
        /// Обработка ответа на команду
        /// </summary>
        /// <param name="answer">Строка ответа на команду</param>
        protected virtual void HandleAnswer(string answer)
        {
            if (CommandAnswerCallback != null)
            {
                string handleAnswerError = CommandAnswerCallback.Invoke(answer);
                if (!string.IsNullOrEmpty(handleAnswerError))
                {
                    throw new Exception(handleAnswerError);
                }
            }
                       
        }


        /// <summary>
        /// Подготовка команды
        /// </summary>
        /// <param name="comStringBase">Базовая строка команды</param>
        /// <returns></returns>
        protected virtual string PrepareCommand(string comStringBase)
        {
            return comStringBase;            //добавить символ 0x0D к базовой строке
        }
    }

    /// <summary>
    /// Реализация установки реального времени панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SetTimeCommand : SimpleCommand
    {
        /// <summary>
        /// Подготовка команды
        /// </summary>
        /// <param name="comString"></param>
        /// <returns></returns>
        protected override string PrepareCommand(string comString)
        {
            DateTime dtNow = DateTime.Now;                                      //получение текущей даты/времени
            
            //получение форматированной строки текущей даты/времени
            string formattedDt = string.Format("{0:HHmmssddMMyyyy}", dtNow);    
            return $"{comString}{formattedDt}";
        }

    }


    /// <summary>
    /// Реализация команды установки типа драйвера панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SetDriverTypeCommand : SimpleCommandPicker
    {
        /// <summary>
        /// Подготовка команды
        /// </summary>
        /// <param name="comString"></param>
        /// <returns></returns>
        protected override string PrepareCommand(string comString)
        {
            //добавление к базовой строке команды добавочной строки для не-AT команд
            return $"{ComString}{SelectedItem.AddString}";
        }
    }

    [Android.Runtime.Preserve(AllMembers = true)]
    public class GetVersionCommand : SimpleCommand
    {
        protected override string PrepareCommand(string comString)
        {
            //добавление к базовой строке команды добавочной строки для не-AT команд
            return comString;
        }

        protected override async void HandleAnswer(string answer)
        {
            answer = answer.TrimEnd(new char[] { '\r', ';' });
            await App.Current.MainPage.DisplayAlert("Сообщение",
                                                $"Версия ПО Arduino {answer}.", "Ok");
        }
    }


    /// <summary>
    /// Реализация установки типа панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SetPanelTypeCommand : SimpleCommandPicker
    {
        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <returns></returns>
        protected override async Task ExecuteCommand()
        {

            string command = $"{ComString}{SelectedItem.AddString}";      //команда установки типа для панели

            List<string> packet = new List<string>
            {
                SelectedItem.AddStringPacket,
                "AT+RESET"
            };
            //если число выбранных панелей для установки равно 1
            if (SelectedPanels.Count == 1)
            {
                //отправить команду и пакет на панель
                await SendCommandAndPacketForConnectedDevice(command, packet);
                await App.Current.MainPage.DisplayAlert("Сообщение",
                                                $"Тип успешно задан.", "Ok");

            }
            else
            {
                //TODO - еще не реализовано для нескольких панелей
                foreach (var panel in SelectedPanels)
                {
                    await SendCommandAndPacketForDevice(panel, command, packet);

                }
                await App.Current.MainPage.DisplayAlert("Сообщение",
                                                $"Тип успешно установлен для {SelectedPanels.Count} панелей(и).", "Ok");

            }

        }

        /// <summary>
        /// После успешного выполнения команды
        /// </summary>
        /// <returns></returns>
        protected override async Task AfterSuccessCommandExecAsync()
        {
            //разорвать сопряжение с устройством
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            await btConnector.DisconnectDeviceAsync();

            //перейти на страницу с списком стен
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
        }

    }

    /// <summary>
    /// Реализация установки имени стены
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SetPanelWallNameCommand : SimpleCommandTextbox
    {

        /// <summary>
        /// Реализация команды
        /// </summary>
        /// <returns></returns>
        protected override async Task ExecuteCommand()
        {
            //получение кодировки Windows-1251 для сохранения имени стены в рекламных сообщениях Bluetooth модулей
            Encoding win1251 = Encoding.GetEncoding("Windows-1251");
            //получение байтов имени
            byte[] nameBytes = win1251.GetBytes(Text);

            //если имя короче максимально допустимого, то дополнить нулями до максимальной длины
            if (nameBytes.Length < MaxLength)
            {
                nameBytes = nameBytes.Concat(new byte[MaxLength - nameBytes.Length]).ToArray();
            }
            /*если каждый 4й байт нулевой - задать равным 0x01 - необходимо, чтобы часть GUID,
            состоящая из 4х байт не имела нулевого значения, иначе не получится установить ее*/

            nameBytes[3] = nameBytes[3] == 0 ? (byte)0x01 : nameBytes[3];
            nameBytes[7] = nameBytes[7] == 0 ? (byte)0x01 : nameBytes[7];
            nameBytes[11] = nameBytes[11] == 0 ? (byte)0x01 : nameBytes[11];


            //получение первых 3х АТ-команд для модуля, содержащих имя стены
            string idPart1 = $"AT+IBE0{nameBytes[0].ToString("X2")}{nameBytes[1].ToString("X2")}{nameBytes[2].ToString("X2")}{nameBytes[3].ToString("X2")}";
            string idPart2 = $"AT+IBE1{nameBytes[4].ToString("X2")}{nameBytes[5].ToString("X2")}{nameBytes[6].ToString("X2")}{nameBytes[7].ToString("X2")}";
            string idPart3 = $"AT+IBE2{nameBytes[8].ToString("X2")}{nameBytes[9].ToString("X2")}{nameBytes[10].ToString("X2")}{nameBytes[11].ToString("X2")}";

            //формирование пакета АТ-команд
            List<string> packet = new List<string>
                {
                    idPart1,
                    idPart2,
                    idPart3,
                    "AT+RESET"
                };
            //отправка пакета АТ-команд
            if (SelectedPanels.Count == 1)
            {
                
                await SendCommandAndPacketForConnectedDevice(null, packet);
                await App.Current.MainPage.DisplayAlert("Сообщение",
                                                $"Имя стены для панели успешно задано.", "Ok");
                
            }
            else
            {
                //TODO реализовать
                throw new Exception("Эта команда недоступна для выполнения на нескольких панелях.");
            }

        }

        /// <summary>
        /// После выполнения AT-команд
        /// </summary>
        /// <returns></returns>
        protected override async Task AfterSuccessCommandExecAsync()
        {
            //разорвать сопряжение с устройством, перейти на страницу с списком стен
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            await btConnector.DisconnectDeviceAsync();

            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
        }

    }

    /// <summary>
    /// Реализация установки имени панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SetPanelNameCommand : SimpleCommandTextbox
    {
        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <returns></returns>
        protected override async Task ExecuteCommand()
        {
            //формирование пакета
            
            List<string> packet = new List<string>
                {
                    ComString + Text,                    
                    "AT+RESET"
                };
            //отправка пакета АТ-команд
            if (SelectedPanels.Count == 1)
            {
                
                await SendCommandAndPacketForConnectedDevice(null, packet);
                await App.Current.MainPage.DisplayAlert("Сообщение",
                                                $"Имя панели успешно задано.", "Ok");

            }
            else
            {
                throw new Exception("Эта команда недоступна для выполнения на нескольких панелях.");
            }

        }

        /// <summary>
        /// Дополнительная проверка вводимого в Entry имени панели
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override string GetAdditionalInputError(string value)
        {
            //Если имя не набрано латиницей - вернуть ошибку
            if (!Regex.IsMatch(value, "^[a-zA-Z0-9]*$"))
            {
                return "Значение должно состоять из символов английского алфавита или цифр";
            }
            return "";
        }

        /// <summary>
        /// После успешного выполнения команды
        /// </summary>
        /// <returns></returns>
        protected override async Task AfterSuccessCommandExecAsync()
        {
            //разрыв сопряжения
            var btConnector = SimpleIoc.Default.GetInstance<IBluetoothConnector>();
            await btConnector.DisconnectDeviceAsync();
            //переход на страницу со списком стен
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
        }


    }

    /// <summary>
    /// Реализация отправки команд, в которых данные вводятся слайдером
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class SimpleCommandSlider : SimpleCommand
    {
        /// <summary>
        /// Дополнительная команда получения усилия актуаторов с панели 
        /// </summary>
        public GetFetchCommand FetchCommand { get; set; }
        

        public ICommand OnValueChangedCommand { get; set; }
        private double _valu;
        /// <summary>
        /// Значение положения слайдера
        /// </summary>
        public double SliderValue
        {
            get { return _valu; }
            set
            {
                value = Math.Round(value);                           //округление к целому                
                ValVisible = $"{value.ToString()} {Prefix}";  //задание отображаемого значения слайдера
                Set<double>(nameof(SliderValue), ref _valu, value);
            }
        }

        /// <summary>
        /// Число цифр (значение слайдера), которые должны следовать после базовой части команды
        /// </summary>
        public int CommandParamDigitsNum { get; set; }
       

        private string _valVisible;
        /// <summary>
        /// Отображаемое значение слайдера
        /// </summary>
        public string ValVisible
        {
            get { return _valVisible; }
            set
            {
                Set<string>(nameof(ValVisible), ref _valVisible, value);
            }
        }

        /// <summary>
        /// Размерность значения
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Минимум положения слайдера
        /// </summary>
        private double _minimum;

        public double Minimum
        {
            get { return _minimum; }
            set
            {
                Set<double>(nameof(Minimum), ref _minimum, value);
            }
        }


        private double _maximum;
        /// <summary>
        /// Максимум положения слайдера
        /// </summary>
        public double Maximum
        {
            get { return _maximum; }
            set
            {
                Set<double>(nameof(Maximum), ref _maximum, value);
            }
        }


        /// <summary>
        /// Подготовка команды
        /// </summary>
        /// <param name="comString"></param>
        /// <returns></returns>
        protected override string PrepareCommand(string comString)
        {
            //базовая строка команды склеивается с значением слайдера, приведенному согласно заданному количеству цифр
            string digitsFormat = $"D{CommandParamDigitsNum.ToString()}";
            return $"{ComString}{((int)SliderValue).ToString(digitsFormat)}";
        }
        
        public SimpleCommandSlider(string prefix, double max, double min)
        {
            Prefix = prefix;            //установка префикса            
            Maximum = max;               //установка максимального значения
            Minimum = min;                //установка минимума в 0
            
        }        

        /// <summary>
        /// Выполнить дополнительную команду получения усилия актуаторов с панели
        /// </summary>
        /// <returns></returns>
        public override async Task<bool> ExecAdditionalCommand()
        {
            //если доп. команда не инициализирована - выбросить ошибку
            if (FetchCommand == null)
            {
                await App.Current.MainPage.DisplayAlert("Ошибка",
                                                    "Дополнительная команда еще не инициализирована.", "Ok");
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()));
                return false;
            }
            
            //обратный вызов метода обработки ответа на команду запроса усилия актуатора
            FetchCommand.CommandAnswerCallback = new Func<string,string>((commandResponse) =>
              {
                  //подрезать лишние символы в конце, убрать признак команды в ответе
                  commandResponse = commandResponse.TrimEnd(new char[] { '\r', ';' });
                  commandResponse = commandResponse.Replace(FetchCommand.ComString, "");

                  //преобразовать оставшуюся чать в значение усилия
                  if (!int.TryParse(commandResponse, out int fetch))
                  { 
                      //вернуть описание ошибки
                      return $"Неверный ответ на {FetchCommand.Description}.";
                  }
                  //установить ползунок слайдера в значение усилия
                  SliderValue = fetch;
                  //вернуть пустую строку ошибки
                  return "";
              });
            //выполнить доп. команду и вернуть результат
            return await FetchCommand.OnExecCommand() ? true : false;               
        }
        
    }


    /// <summary>
    /// Реализация получения усилия актуатора
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class GetFetchCommand : SimpleCommand
    {        

        protected override string PrepareCommand(string comString)
        {
            return $"{ComString}?";
        }       

    }

    /// <summary>
    /// Реализация получения истории панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class GetHistoryCommand : SimpleCommandTextbox
    {
        
        /// <summary>
        /// Структура признаков истории
        /// </summary>
        private readonly List<HistoryMarker> HMarkers;

        [Preserve]
        public GetHistoryCommand()
        {
            IsNumbersOnly = true;                   //установка флага, что вводиться будут только цифры в Entry
            HMarkers = new List<HistoryMarker>();   //инициализация Структуры признаков истории
            #region Структура признаков истории
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "TO",
                Description = "Выдвижение верхнего актуатора"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "TC",
                Description = "Задвижение верхнего актуатора"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "TT",
                Description = "Остановка верхнего актуатора по таймауту",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "TF",
                Description = "Остановка верхнего актуатора по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "TI",
                Description = "Остановка верхнего актуатора по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "BO",
                Description = "Выдвижение нижнего актуатора"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "BC",
                Description = "Задвижение нижнего актуатора"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "BT",
                Description = "Остановка нижнего актуатора по таймауту",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "BF",
                Description = "Остановка нижнего актуатора по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "BI",
                Description = "Остановка нижнего актуатора по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "1O",
                Description = "Выдвижение бокового актуатора1"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "1C",
                Description = "Задвижение бокового актуатора1"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "1T",
                Description = "Остановка бокового актуатора1 по таймауту",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "1F",
                Description = "Остановка бокового актуатора1 по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "1I",
                Description = "Остановка бокового актуатора1 по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "2O",
                Description = "Выдвижение бокового актуатора2"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "2C",
                Description = "Задвижение бокового актуатора2"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "2T",
                Description = "Остановка бокового актуатора2 по таймауту",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "2F",
                Description = "Остановка бокового актуатора2 по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "2I",
                Description = "Остановка бокового актуатора2 по току",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Расстояние выдвижения/задвижения"),
                    new HistoryMarkerValue(2,"Установленное усилие актуатора")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "PC",
                Description = "Подключение следующей панели"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "PD",
                Description = "Отключение следующей панели"
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "ST",
                Description = "Начало работы панели"
            });            
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "FT",
                Description = "Установленное макс. усилие верхнего актуатора",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(2,"Усилие")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "FB",
                Description = "Установленное макс. усилие нижнего актуатора",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(2,"Усилие")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "F1",
                Description = "Установленное макс. усилие бокового актуатора1",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(2,"Усилие")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "F2",
                Description = "Установленное макс. усилие бокового актуатора2",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(2,"Усилие")
                }
            });
            HMarkers.Add(new HistoryMarker()
            {
                Marker = "GL",
                Description = "Установленная степень затемненности стекла.",
                Values = new List<HistoryMarkerValue>()
                {
                    new HistoryMarkerValue(3,"Затемнение")
                }
            });
            #endregion
        }

        /// <summary>
        /// Заданное пользователем число записей истории
        /// </summary>
        private uint HistoryLen { get; set; }

        /// <summary>
        /// Подготовка команды
        /// </summary>
        /// <param name="comString"></param>
        /// <returns></returns>
        protected override string PrepareCommand(string comString)
        {
            //Склеивание базовой строки команды с заданным числом записей истории
            return $"{comString}{HistoryLen.ToString("D3")}";
        }


        private const int MaxHistoryLen = 100;                  //максимальное число записей истории
        /// <summary>
        /// Дополнительная проверка вводимого в Entry числа запрашиваемого числа записей истории
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override string GetAdditionalInputError(string value)
        {
            byte result;
            string errMess = $"Значение должно быть числом от 1 до {MaxHistoryLen}";
            if (!byte.TryParse(value, out result))
            {
                return errMess;
            }

            if ((result < 1) || (result > MaxHistoryLen))
            {
                return errMess;
            }
            HistoryLen = result;
            return "";
        }

        //число символов в записи истории, которые содержат данные даты\времени события
        private const int DateTimeStrLen = 12;
        //число символов, содержащих признак
        private const int MarkerStrLen = 2;
        /// <summary>
        /// Обработка полученной строки с историей
        /// </summary>
        /// <param name="answer"></param>
        protected override async void HandleAnswer(string answer)
        {
            try
            {
                //подрезать с конца завершающий символ сообщения истории и завершающий символ записи истории
                answer = answer.TrimEnd(new char[] { '\r', ';' });
                //разбить на сообщения историю
                List<string> historyNotes = answer.Split('\r').ToList();
                //удалить сообщения, не относящиеся к истории (не начинаются с символа 'H')
                historyNotes.RemoveAll(f => !f.StartsWith("H"));
                
                //Проверить, что число записей истории равно запрошенному
                if (HistoryLen != historyNotes.Count)
                {
                    throw new Exception($"Число запрошенных записей истории {HistoryLen}, получено {historyNotes.Count}.");
                }

                //инициализация списка данных истории
                List<HistoryData> history = new List<HistoryData>();
                //Для каждой записи истории...
                foreach (var note in historyNotes)
                {
                    string timePart;
                    string markerPart;
                    try
                    {
                        timePart = note.Substring(1, DateTimeStrLen);                   //часть содержащая время
                        markerPart = note.Substring(1 + DateTimeStrLen, MarkerStrLen);  //часть содержащая признак
                    }
                    catch (Exception)
                    {
                        throw new Exception("Не удалось выделить подстроку времени или признака в записи истории.");
                    }

                    DateTime dt;
                    try
                    {
                        //преобразовать отформатированную строку времени в DateTime
                        dt = DateTime.ParseExact(timePart, "HHmmssddMMyy",
                                  CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Не удалось преобразовать значение времени в записи истории.");
                    }

                    string lastPart;
                    try
                    {
                        //получение заключительной части записи истории (различается для каждого признака)
                        int lastPartStartIndex = 1 + DateTimeStrLen + MarkerStrLen;
                        lastPart = note.Substring(lastPartStartIndex, note.Length - lastPartStartIndex);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Не удалось выделить выделить завершающую часть истории сообщения.");
                    }

                    //поиск известного признака
                    HistoryMarker markerMod = HMarkers.SingleOrDefault(f => f.Marker == markerPart);
                    if (markerMod == null)
                    {
                        throw new Exception($"Неизвестный признак {markerPart}");
                    }
                    //если признак известен, то получить данные для него из заключительной части записи
                    markerMod.GetMarkerValuesFromString(lastPart);
                    //добавить запись в данные
                    history.Add(new HistoryData()
                    {
                        Date = dt,
                        ActionDescr = markerMod.Description,
                        AdditionalInfo = markerMod.GetValuesInfo(),
                        Marker = markerMod.Marker
                    });
                }
                //сохранить данные истории в хранилище
                var dataStore = SimpleIoc.Default.GetInstance<IUserDataStore>();
                dataStore.SetHistoryData(history);
            }
            catch (Exception ex)        //при ошибке обработки данных истории вывести сообщение и вернуться к списку стен
            {
                await App.Current.MainPage.DisplayAlert("Ошибка обработки данных истории",
                                                    ex.Message, "Ok");
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()));
                return;
            }
            finally
            {
                Messenger.Default.Send(new BusyMessage("", false));
            }
            //при успешной обработке истории перейти на страницу отображения истории
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new HistoryPage()));

        }
    }
}
