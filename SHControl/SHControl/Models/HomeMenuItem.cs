﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Models
{
    /// <summary>
    /// Типы элементов бокового меню главной страницы приложения
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public enum MenuItemType
    {
        Version,                    //версия приложения
        LogOut,                     //логаут
        ManageUser                  //редактирование пользователей
    }
    /// <summary>
    /// Элемент бокового меню главной страницы приложения
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class HomeMenuItem
    {
        /// <summary>
        /// Тип
        /// </summary>
        public MenuItemType Id { get; set; }
        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Реализация сравнения для реализации удаления элемента из коллекции Observable
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var item = (HomeMenuItem)obj;
            return this.Id == item.Id;
        }

        /// <summary>
        /// Получение Хэш для реализации удаления элемента из коллекции Observable
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Id.ToString().GetHashCode();
        }
    }
}
