﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Plugin.BLE.Abstractions.Contracts;
using SHControl.Messages;
using SHControl.Services;
using SHControl.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SHControl.Models
{
    /// <summary>
    /// Тип панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public enum PanelType
    {
        Unsetted,               //не задана
        Simple,                 //простая
        Telescopic,             //телескопическая
        SimpleSmartGlass,       //простая с смарт-стеклом
        TelescopicSmartGlass    //телескопическая с смарт-стеклом
    }

    /// <summary>
    /// Панель
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class DevPanel : ObservableObject
    {
        /// <summary>
        /// Команда выбора панели пользователем
        /// </summary>
        public ICommand PanelSelectCommand { get; set; }
        /// <summary>
        /// Идентификатор панели
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Характеристика службы Bluetooth
        /// </summary>
        public ICharacteristic UartChar { get; set; }
        /// <summary>
        /// Имя панели
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Имя стены
        /// </summary>
        public string WallName { get; set; }
        
        /// <summary>
        /// Выбрана ли стена
        /// </summary>
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                
                Set(nameof(IsSelected), ref _isSelected, value);
               
            }
        }

        /// <summary>
        /// Выбрана ли стена чекбоксом для последующих операций
        /// </summary>
        
        private bool _isSelectedByCheckBox;

        public bool IsSelectedByCheckBox
        {
            get { return _isSelectedByCheckBox; }
            set
            {
                Set(nameof(IsSelectedByCheckBox), ref _isSelectedByCheckBox, value);
                Messenger.Default.Send(new NotificationMessage<bool>(value, "IsSelectedByCheckBox"));
                
            }
        }

        /// <summary>
        /// Тип панели
        /// </summary>
        private PanelType _panelType;

        public PanelType PanType
        {
            get { return _panelType; }
            set
            {
                Set(nameof(PanType), ref _panelType, value);
            }
        }

        /// <summary>
        /// Флаг того, что был тап по панели
        /// </summary>
        private bool isTapped;
        public bool IsTapped
        {
            get { return isTapped; }
            set
            {
                Set(nameof(IsTapped), ref isTapped, value);
            }
        }
               

        /// <summary>
        /// Bluetooth устройство панели
        /// </summary>
        public IDevice Instance { get; set; }
        /// <summary>
        /// Измененное Rssi панели
        /// </summary>
        public int Rssi { get; set; }

        public DevPanel(Guid id)
        {   
            //при выборе панели пользователем - отправить сообщение
            PanelSelectCommand = new RelayCommand(()=> 
            { Messenger.Default.Send(new UserPanelSelectMessage() { Panel = this }); });
        }
        
    }
}
