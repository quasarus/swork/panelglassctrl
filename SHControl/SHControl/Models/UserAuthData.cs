﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using SHControl.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using System.Xml.Serialization;

namespace SHControl.Models
{
    /// <summary>
    /// Данные авторизации пользователей и админа
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    [Serializable]
    public class UserAuthData
    { 
        /// <summary>
        /// Команда удаления пользователя
        /// </summary>
        [XmlIgnore]        
        public ICommand UserDeleteCommand { get; set; }
        /// <summary>
        /// Описание пользователя
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Является ли пользователь админом
        /// </summary>
        public bool IsAdmin { get; set; }

        public UserAuthData()
        {
            //при запросе на удаление пользователя разослать сообщение
            UserDeleteCommand = new RelayCommand(() => 
            {
                Messenger.Default.Send(new AuthUserDeleteMessage(Login));
            });
        }
    }
}
