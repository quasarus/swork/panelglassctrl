﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SHControl.Views;
using SHControl.ViewModels;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SHControl
{
    public partial class App : Xamarin.Forms.Application
    {
        private static ViewModelLocator _locator;

        public static ViewModelLocator Locator
        {
            get
            {
                return _locator ?? (_locator = new ViewModelLocator());
            }
        }

        public App()
        {            
            InitializeComponent();            
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }        

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
