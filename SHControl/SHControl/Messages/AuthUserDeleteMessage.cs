﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Messages
{
    /// <summary>
    /// Сообщение мессенжера MVVM о удалении авторизационных данных пользователя
    /// </summary>
    public class AuthUserDeleteMessage
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        public AuthUserDeleteMessage(string login)
        {
            Login = login;
        }
    }
}
