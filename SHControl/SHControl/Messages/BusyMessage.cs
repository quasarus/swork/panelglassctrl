﻿using SHControl.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Messages
{
    /// <summary>
    /// Сообщение мессенжера MVVM о длительном процессе
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class BusyMessage
    {
        /// <summary>
        /// Идет ли длительный процесс
        /// </summary>
        public bool IsBusy { get; set; }
        /// <summary>
        /// Уведомление о идущем длительном процессе
        /// </summary>
        public string Message { get; set; }

        public BusyMessage(string mess, bool isBusy)
        {
            Message = mess;
            IsBusy = isBusy;
        }
    }
}
