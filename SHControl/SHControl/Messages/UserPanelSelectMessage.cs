﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Messages
{
    /// <summary>
    /// Сообщение мессенжера MVVM о выбранной пользователем панели
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class UserPanelSelectMessage
    {
        /// <summary>
        /// Выбранная пользователем панель
        /// </summary>
        public DevPanel Panel { get; set; }
    }
    
}
