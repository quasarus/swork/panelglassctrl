﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using SHControl.Models;
using System.Linq;
using Plugin.BLE.Abstractions.EventArgs;
using GalaSoft.MvvmLight.Messaging;
using Xamarin.Forms;
using SHControl.Views;
using SHControl.Messages;
using SHControl.ViewModels;
using Plugin.BLE.Abstractions.Exceptions;

namespace SHControl.Services
{
    /// <summary>
    /// Служба соединения с устройством Bluetooth и обмена данными
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class BluetoothConnector : IBluetoothConnector,IDisposable
    {
        private CancellationTokenSource Сts;                        //источник токена отмены текущей операции

        //строка первых 3х байт идентификатора службы Bluettooth UART, которые должны быть всегда 0, если модуль правильно сконфигурирован
        private const string ServiceIdPartOne = "000000";
        //строка остальных байт идентификатора службы Bluettooth UART
        private const string ServiceIdPartTwo = "-0000-1000-8000-00805f9b34fb";
        //строка идентификатора характеристики службы Bluettooth UART
        private const string CharacteristicId = "00000001-0000-1000-8000-00805f9b34fb";

        //таймаут ожидания подтверждения команды
        private const int CommandAckTimeoutMs = 3000;

        //private const int AtPacketDisconnectWaitTime = 2000;
        //private const int AtPacketExecWaitTime = 7000;
        //число попыток переподключения по ошибке - для избежания отображения ошибки GATT
        private const int ReconnectAttempts = 2;
        //панель, с которой есть сопряжение
        private DevPanel connDevice;

        //обработчики событий потери сопряжения и разрыва сопряжения
        private EventHandler<DeviceErrorEventArgs> ConnLostEventHandler;
        private EventHandler<DeviceEventArgs> DisconnectedEventHandler;

        /// <summary>
        /// Асинхронное соединение с панелью
        /// </summary>
        /// <param name="device">Панель</param>
        /// <returns></returns>
        public async Task<bool> ConnectToDeviceAsync(DevPanel device)
        {
            //получение источника токена отмены
            Сts = GetCts(Сts);      
            ConnectParameters parameters = new ConnectParameters();
            IAdapter adapter = CrossBluetoothLE.Current.Adapter;

            //попытаться подконнектиться, если ошибка - коннект еще раз - ращита от ошибки GATT Err
            try
            {
                await adapter.ConnectToDeviceAsync(device.Instance, parameters, Сts.Token);
            }
            catch (DeviceConnectionException)
            {
                await adapter.ConnectToDeviceAsync(device.Instance, parameters, Сts.Token);
            }            
            
            
            //проверка, что сопряжение есть
            if (device.Instance.State != DeviceState.Connected)
            {
                return false;
            }            

            //получение списка сервисов
            IList<IService> allServs = await device.Instance.GetServicesAsync(Сts.Token);
            //проверка что задача не была отменена
            if (Сts.Token.IsCancellationRequested)
            {
                return false;
            }

            //получение UART сервиса
            IService uartServ = null;            
            uartServ = allServs.SingleOrDefault(v => v.Id.ToString().
            StartsWith(ServiceIdPartOne) && v.Id.ToString().Contains(ServiceIdPartTwo));
            
            //проверка, что сервис существует
            if (uartServ == null)
            {
                await DisconnectDeviceAsync();
                throw new Exception($"Не удалось обнаружить" +
                    $" требуемую службу на Bluetooth устройстве {device.Name}");
            }

            //получение списка характеристик
            IList<ICharacteristic> allChars = await uartServ.GetCharacteristicsAsync();
            ICharacteristic uartChar = null;
            
            //получение характеристики по ее UUID
            uartChar = allChars.SingleOrDefault(g => g.Id.ToString() == CharacteristicId);

            //проверка, что характеристика существует
            if (uartChar == null)
            {
                await DisconnectDeviceAsync();
                throw new Exception($"Не удалось обнаружить" +
                    $" требуемую характеристику в службе на Bluetooth устройстве {device.Name}");
            }

            device.UartChar = uartChar;     //сохранение характеристики             
            connDevice = device;            //сохранение объекта устройства Bluetooth

            //инициализация обработчиков событий потери сопряжения и разрыва сопряжения
            ConnLostEventHandler = new EventHandler<DeviceErrorEventArgs>(Adapter_DeviceConnectionLost);
            DisconnectedEventHandler = new EventHandler<DeviceEventArgs>(Adapter_DeviceDisconnected);

            //подписка на событияпотери сопряжения и разрыва сопряжения
            adapter.DeviceConnectionLost += ConnLostEventHandler;
            adapter.DeviceDisconnected += DisconnectedEventHandler;
            return true;
        }

        /// <summary>
        /// По событию отключению устройства
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Adapter_DeviceDisconnected(object sender, DeviceEventArgs e)
        {
            //отписка от события
            IAdapter adapter = CrossBluetoothLE.Current.Adapter;
            adapter.DeviceDisconnected -= DisconnectedEventHandler;

            //уведомление в GUI
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                                                    $"Сопряжение с устройством разорвано", "Ok");
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
            });            
            
        }

        public bool IsDeviceConnected()
        {
            if (connDevice == null)
            {
                return false;
            }
            else
            {
                return connDevice.Instance.State == DeviceState.Connected ? true : false;
            }
        }

        /// <summary>
        /// По событию разрыва соединения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Adapter_DeviceConnectionLost(object sender, DeviceErrorEventArgs e)
        {
            //отписка от события
            IAdapter adapter = CrossBluetoothLE.Current.Adapter;
            adapter.DeviceConnectionLost -= ConnLostEventHandler;

            //уведомление в GUI
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                                                    $"Потеряна связь с сопряженным устройством.", "Ok");
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
            });

            
        }

        /// <summary>
        /// Асинхронный разрыв соединения с панелью
        /// </summary>
        /// <returns></returns>
        public async Task DisconnectDeviceAsync()
        {
            await ResetDevicePairing();
        }

        /// <summary>
        /// Получение источника токена отмены текущей операции
        /// </summary>
        /// <param name="cts"></param>
        /// <returns></returns>
        private CancellationTokenSource GetCts(CancellationTokenSource cts)
        {
            //если исходный источник токена не инициализирован - инициализировать
            if (cts == null)
            {
                return new CancellationTokenSource();
            }
            else
            {
                //проверить, был ли отменен источник токена
                if (cts.IsCancellationRequested)
                {
                    //если отменен - разрушить и инициализировать новый
                    cts.Dispose();
                    return new CancellationTokenSource(); ;
                }
                
            }
            return cts;
        }


        //буфер данных, полученных через характеристику
        private string CharRxData;  
        
        /// <summary>
        /// Отправить команду асинхронно
        /// </summary>
        /// <param name="command">Содержимое</param>
        /// <param name="isEndOfAtPacket">Если true - содержимое AT-пакет, иначе просто команда</param>
        /// <returns></returns>
        public async Task<string> SendCommandAsync(string command, bool isEndOfAtPacket)
        {       
            //проверка, что уже установлено соединение с панелью
            if (connDevice == null)
            {
                throw new Exception("Сопряжение с панелью еще не установлено.");
            }

            //проверка состояния, что есть сопряжение с панелью
            if (connDevice.Instance.State != DeviceState.Connected)
            {
                throw new Exception($"Нет сопряжения с панелью {connDevice.Name}.");
            }

            //проверка характеристики
            ICharacteristic charact = connDevice.UartChar;
            if (charact == null)
            {
                throw new Exception($"Характеристика устройства для записи в UART недоступна.");
            }

            //получение байтового буфера команд
            command = $"\r{command}\r";
            byte[] comBytes = Encoding.ASCII.GetBytes(command);

            //обнуление буфера
            CharRxData = "";
            CancellationTokenSource cts = new CancellationTokenSource();
            //инициализация обработчика события обновления значения характеристики
            EventHandler<CharacteristicUpdatedEventArgs> hUpdHandler=
                new EventHandler<CharacteristicUpdatedEventArgs>((o,e)=>
                {
                    //помещение принятых данных в буфер
                    CharRxData += Encoding.ASCII.GetString(e.Characteristic.Value);
                    //проверка наличия завершающих символов
                    if (CharRxData.Contains(";"))
                    {
                        cts.Cancel();
                    }
                });
            //подписка на событие обновления значения характеристики
            charact.ValueUpdated += hUpdHandler;

            /*если будет отправляться АТ-пакет, то сразу отписаться от событий
            разрыва связи и потери сопряжения, т.к связь все равно будет потеряна при обработке AT-пакета*/
            if (isEndOfAtPacket)
            {
                IAdapter adapter = CrossBluetoothLE.Current.Adapter;
                adapter.DeviceConnectionLost -= ConnLostEventHandler;
                adapter.DeviceDisconnected -= DisconnectedEventHandler;
            }
            //старт слежения за изменением значения характеристики
            await charact.StartUpdatesAsync();
            //запись в характеристику (должна выполняться в основном потоке)
            await charact.WriteAsync(comBytes);
            
            //ожидание ответа
            await Task.Delay(CommandAckTimeoutMs, cts.Token).ContinueWith((tsk)=> 
            {
                if (tsk.Status != TaskStatus.Canceled)
                {
                    string errorMess = isEndOfAtPacket ? "Таймаут приема подтверждения" +
                    " на отправку пакета АТ-команд." : "Таймаут приема подтверждения на команду.";
                    
                    throw new Exception(errorMess);
                }
            });
            
            //останов слежения за обновлением значения характеристики
            await charact.StopUpdatesAsync();
            charact.ValueUpdated -= hUpdHandler;
            
            return CharRxData;
        }
               
        /// <summary>
        /// Получить панель, с которой было соединение
        /// </summary>
        /// <returns></returns>
        public DevPanel GetConnectedPanel()
        {
            return connDevice;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual async void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                await ResetDevicePairing();

                
                
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        private async Task ResetDevicePairing()
        {
            IAdapter adapter = CrossBluetoothLE.Current.Adapter;
            adapter.DeviceDisconnected -= DisconnectedEventHandler;
            adapter.DeviceConnectionLost -= ConnLostEventHandler;

            if (connDevice != null)
            {
                IDevice device = connDevice.Instance;

                if (device.State == DeviceState.Connected)
                {
                    await adapter.DisconnectDeviceAsync(device);
                }
            }

            try
            {
                Сts?.Cancel();
                Сts?.Dispose();
                
            }
            catch (Exception)
            {
                Сts = null;
            }
            
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BluetoothConnector() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        
        #endregion
    }
}
