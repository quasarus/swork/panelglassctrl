﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SHControl.Services
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public interface IBToothSearcher
    {
        void CancelCurrentOperation();
        void StartSearchDeviceAsync(Action<DevPanel> foundedPanelCallback, Action<bool> scanStartEndCallback);
    }
}
