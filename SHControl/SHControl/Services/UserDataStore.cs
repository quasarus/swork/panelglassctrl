﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using SHControl.Helpers;
using SHControl.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Xamarin.Forms.Xaml;


namespace SHControl.Services
{
    /// <summary>
    /// Тип состояния авторизации
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public enum AuthStatusType
    {
        BeforeProcessOfAuth,            //начало процесса авторизации
        InProcessOfAuth,                //в процессе авторизации
        NonAuthorized,                  //при неудачной авторизации                
        AuthAsAdmin,                    //при успешной авторизации как админ
        AuthAsUser                      //при успешной авторизации как пользователь
    }
    /// <summary>
    /// Тип режима работы приложения
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public enum AppModeType
    {
        User,                           //пользовательский
        Tech                            //технологический
    }

    /// <summary>
    /// Хранилище данных
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class UserDataStore : IUserDataStore
    {  
        /// <summary>
        /// Состояние авторизации
        /// </summary>
        private AuthStatusType AuthorizationStatus { get; set; }

        //логин и пароль админа по умолчанию
        private const string AdmDefaultLogin = "admin";
        private const string AdmDefaultPassword = "admin";

        //ключи для хранения сохраненных логина и пароля в хранилище настроек
        private const string SavedLoginKey = "SLoginKey";
        private const string SavedPasswordKey = "SPasswordKey";

        //ключ для хранения значения автологина в хранилище настроек
        private const string AutoAutorizeKey = "AutoAKey";

        //ключ для хранения авторизационных данных в хранилище настроек
        private const string AuthDataKey = "ADKey";

        //описание текущего пользователя
        public string UserDescription { get; set; }

        //текущий заданный режим работы приложения
        private AppModeType AppMode { get; set; }
        //выбранные панели для работы
        private List<DevPanel> SelectedPanels { get; set; }        

        //данные истории
        private List<HistoryData> HData { get; set; }
        
        private static ISettings AppSettings =>
            CrossSettings.Current;

        /// <summary>
        /// Асинхронное получение списка пользователей
        /// </summary>
        /// <returns></returns>
        public Task<List<UserAuthData>> GetUsersAsync()
        {
            return Task<List<UserAuthData>>.Factory.StartNew(() => 
            {
                //десериализация объекта, хранящего список пользователей
                var users = DeSerializeFromString(AppSettings.GetValueOrDefault(AuthDataKey, ""));
                //расшифровка имени пользователей
                foreach (var usr in users)
                {
                    usr.Password = "";
                    usr.Login = ManagedAes.DecryptStringAES(usr.Login);
                }
                
                return users.Where(f=>!f.IsAdmin).ToList();
            });
            
        }

        /// <summary>
        /// Асинхронная аторизация
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="isRemember">Запоминать ли при дальнейшем входе</param>
        /// <returns></returns>
        public Task<AuthStatusType> Authorise(string login, string password,bool isRemember)
        {
            return Task<AuthStatusType>.Factory.StartNew(() =>
            {
                //получение списка пользователей
                List<UserAuthData> usrAData = DeSerializeFromString(AppSettings.GetValueOrDefault(AuthDataKey, ""));

                //для каждого пользователя
                foreach (var usr in usrAData)
                {
                    //расшифровка логина и пароля и сравнение с введенными
                    if (login == ManagedAes.DecryptStringAES(usr.Login) && password == ManagedAes.DecryptStringAES(usr.Password))
                    {
                        //если необходимо запомнить, то сохранить расшифрованные имя и пароль
                        if (isRemember)
                        {
                            AppSettings.AddOrUpdateValue(SavedLoginKey, ManagedAes.EncryptStringAES(login));
                            AppSettings.AddOrUpdateValue(SavedPasswordKey, ManagedAes.EncryptStringAES(password));
                        }
                        UserDescription = usr.Description;
                        return usr.IsAdmin ? AuthStatusType.AuthAsAdmin : AuthStatusType.AuthAsUser;
                    }
                }
                return AuthStatusType.NonAuthorized;
            });
        }

        

        public UserDataStore()
        {
            UserDescription = "";
            SelectedPanels = new List<DevPanel>();            
        }

        /// <summary>
        /// Получение описания залогоненного пользователя
        /// </summary>
        /// <returns></returns>
        public string GetUserDescription()
        {
            return UserDescription;
        }
        
        /// <summary>
        /// Инициализация данных авторизации
        /// </summary>
        public void InitAuthData()
        {  
            //если еще нет информации о пользователях в хранилище настроек 
            if (!AppSettings.Contains(AuthDataKey))
            {
                //создать акк админа
                List<UserAuthData> usrAData = new List<UserAuthData>();
                usrAData.Add(new UserAuthData()
                {
                    Description = "Администратор",
                    Login = ManagedAes.EncryptStringAES(AdmDefaultLogin),
                    Password = ManagedAes.EncryptStringAES(AdmDefaultPassword),
                    IsAdmin = true
                });
                AppSettings.AddOrUpdateValue(AuthDataKey, SerializeToString(usrAData));
            }

        }

        /// <summary>
        /// получить текущее состояние авторизации
        /// </summary>
        /// <returns></returns>
        public AuthStatusType GetAuthorizationStatus()
        {
            return AuthorizationStatus;
        }

        
        /// <summary>
        /// Сохранить флаг автологина в хранилище настроек
        /// </summary>
        /// <param name="isAuto"></param>
        public void SetAutoAuthorize(bool isAuto)
        {
            AppSettings.AddOrUpdateValue(AutoAutorizeKey, isAuto);
            if (!isAuto)
            {
                AppSettings.AddOrUpdateValue(SavedLoginKey, "");
                AppSettings.AddOrUpdateValue(SavedPasswordKey, "");
            }
        }

        /// <summary>
        /// проситать настройку автологина из хранилища настроек
        /// </summary>
        /// <returns></returns>
        public bool IsAutoAuthorize()
        {
            return AppSettings.GetValueOrDefault(AutoAutorizeKey, false);
        }

        /// <summary>
        /// Асинхронный автологин
        /// </summary>
        /// <returns></returns>
        public Task<AuthStatusType> AutoAuthoriseAsync()
        {

            return Task<AuthStatusType>.Factory.StartNew(() =>
            {
                //если нет сохраненных данных для автологина, вернуть состояние неудачной авторизации
                string savedLogin = AppSettings.GetValueOrDefault(SavedLoginKey, "");
                string savedPwd = AppSettings.GetValueOrDefault(SavedPasswordKey, "");
                if (string.IsNullOrEmpty(savedLogin) || string.IsNullOrEmpty(savedPwd))
                {
                    return AuthStatusType.NonAuthorized;
                }
                //расшифрованные логин и пароль
                string savedLoginDec = ManagedAes.DecryptStringAES(savedLogin);
                string savedPwdDec = ManagedAes.DecryptStringAES(savedPwd);                

                //расшифровать из хранилища настроек данные авторизации и найти в них пользователя
                List<UserAuthData> usrAData = DeSerializeFromString(AppSettings.GetValueOrDefault(AuthDataKey, ""));
                foreach (var usr in usrAData)
                {
                    if (savedLoginDec == ManagedAes.DecryptStringAES(usr.Login) && savedPwdDec == ManagedAes.DecryptStringAES(usr.Password))
                    {
                        UserDescription = usr.Description;
                        return usr.IsAdmin ? AuthStatusType.AuthAsAdmin : AuthStatusType.AuthAsUser;
                    }
                }
                return AuthStatusType.NonAuthorized;
            });

        }

        /// <summary>
        /// Установить режим работы приложения
        /// </summary>
        /// <param name="mode"></param>
        public void SetAppMode(AppModeType mode)
        {
            AppMode = mode;
        }

        /// <summary>
        /// Получить режим работы приложения
        /// </summary>
        /// <returns></returns>
        public AppModeType GetAppMode()
        {
            return AppMode;
        }

        /// <summary>
        /// Сохранить выбранные для работы панели
        /// </summary>
        /// <param name="panels"></param>
        public void SetSelectedPanels(List<DevPanel> panels)
        {
            SelectedPanels = panels;
        }

        /// <summary>
        /// Получить выбранные для работы панели
        /// </summary>
        /// <returns></returns>
        public List<DevPanel> GetSelectedPanels()
        {
            return SelectedPanels;
        }

        /// <summary>
        /// Получить данные истории
        /// </summary>
        /// <returns></returns>
        public List<HistoryData> GetHistoryData()
        {            
            return HData;
        }

        /// <summary>
        /// Сохранить данные истории
        /// </summary>
        /// <param name="historyData"></param>
        public void SetHistoryData(List<HistoryData> historyData)
        {
            HData = historyData;
        }

        /// <summary>
        /// Добавить нового пользователя асинхронно
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Task AddNewUserAsync(UserAuthData newUser, string login, string password)
        {
            return Task.Factory.StartNew(() =>
            {
                //зашифровать имя пользователя и пароль
                newUser.Login = ManagedAes.EncryptStringAES(login);
                newUser.Password = ManagedAes.EncryptStringAES(password);

                //получить список пользователей из хранилища настроек
                List<UserAuthData> storedUsers = DeSerializeFromString(AppSettings.GetValueOrDefault(AuthDataKey, "")); ;
                //добавить пользователя и добавить сериализованные данные в хранилище настроек
                storedUsers.Add(newUser);
                AppSettings.AddOrUpdateValue(AuthDataKey, SerializeToString(storedUsers));
            });

        }


        /// <summary>
        /// Удалить пользователя асинхронно
        /// </summary>
        /// <param name="login">логин пользователя</param>
        /// <returns></returns>
        public Task RemoveUserAsync(string login)
        {
            return Task.Factory.StartNew(() =>
            {
                //получить список пользователей из хранилища настроек
                List<UserAuthData> storedUsers = DeSerializeFromString(AppSettings.GetValueOrDefault(AuthDataKey, ""));

                //удалить пользователя из данных
                UserAuthData userToDelete = null;
                foreach (var usr in storedUsers)
                {
                    if (ManagedAes.DecryptStringAES(usr.Login) == login)
                    {
                        userToDelete = usr;
                        break;
                    }
                }

                if (userToDelete != null)
                {
                    storedUsers.Remove(userToDelete);
                }
                //добавить сериализованные данные в хранилище настроек
                AppSettings.AddOrUpdateValue(AuthDataKey, SerializeToString(storedUsers));
            });
        }

        

        #region Сериализация и десериализация строк

        public static string SerializeToString(List<UserAuthData> data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<UserAuthData>));

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, data);

                return writer.ToString();
            }
        }

        public static List<UserAuthData> DeSerializeFromString(string data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<UserAuthData>));

            using (TextReader writer = new StringReader(data))
            {
                return (List<UserAuthData>)serializer.Deserialize(writer);                
            }
        }

        


        #endregion


    }
}
