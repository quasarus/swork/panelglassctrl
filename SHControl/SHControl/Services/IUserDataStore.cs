﻿using SHControl.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SHControl.Services
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public interface IUserDataStore
    {
        Task<AuthStatusType> Authorise(string username, string password, bool isRemember);
        
        AuthStatusType GetAuthorizationStatus();

        Task<AuthStatusType> AutoAuthoriseAsync();
        
        void SetAutoAuthorize(bool isAuto);
        Task<List<UserAuthData>> GetUsersAsync();
        Task AddNewUserAsync(UserAuthData newUser, string login, string password);
        Task RemoveUserAsync(string login);
        string GetUserDescription();

        bool IsAutoAuthorize();
        void InitAuthData();

        void SetAppMode(AppModeType mode);
        AppModeType GetAppMode();
        void SetSelectedPanels(List<DevPanel> panels);
        List<DevPanel> GetSelectedPanels();
        List<HistoryData> GetHistoryData();
        void SetHistoryData(List<HistoryData> historyData);
        
    }
}
