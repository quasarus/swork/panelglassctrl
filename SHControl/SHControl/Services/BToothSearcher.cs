﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using SHControl.DependencyServices;
using SHControl.Models;
using Xamarin.Forms;
using System.Linq;

namespace SHControl.Services
{
    /// <summary>
    /// Реализация поиска устройств Bluetooth
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    class BToothSearcher : IBToothSearcher
    {
        private CancellationTokenSource Сts;                          //источник токена отмены текущей операции
        private Action<DevPanel> _foundedPanelCallback;

        private const int BleScanTimeMs = 30000;                      //время сканирования устройств BLE в мс
        private const int ManufacturerDataLen = 25;                   //длина Advertise данных, содержащих данные производителя
        private const int UUID128Size = 16;                           //длина 128-битной GUID 
        private const int UUID16Size = 2;                             //длина 16-битной GUID
        //смещение UUID маяка IBeacon в Advertise данных производителя
        private const int IBeaconUUIDOffset = 4;                      
        private const int WallNameLen = 12;                           //длина имени стены
        /*идентификатор устройства, вещаемый в Advertise. первые 4 байта хранятся в 4х последних байтах
         UUID iBeacon, 2 байта в Major части версии IBeacon, 2 байта в Minor части версии IBeacon*/
        private readonly byte[] DeviceID = { 0x47,0xA5,0xD1,0x50, 0x47,0x01,0x69,0xAF};
        //имя стены по умолчанию при инициализации
        private readonly byte[] DefaultWallNameBytes =
            {0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,0x01,0x00, 0x00, 0x00,0x01};
        //размер Minor Major версии iBeacon
        private const int MajorMinorVerSize = 2;
        //смещение Major в Advertise данных производителя
        private const int MajorVerOffset = IBeaconUUIDOffset + UUID128Size;
        //смещение Minor в Advertise данных производителя
        private readonly int MinorVerOffset = MajorVerOffset + MajorMinorVerSize;
        public void CancelCurrentOperation()
        {
            Сts?.Cancel();            
        }

        /// <summary>
        /// Получение источника токена отмены
        /// </summary>
        /// <param name="cts"></param>
        /// <returns></returns>
        private CancellationTokenSource GetCts(CancellationTokenSource cts)
        {
            //если исходный источник токена не инициализирован - инициализировать
            if (cts == null)
            {
                return new CancellationTokenSource();
            }
            else
            {
                //проверить, был ли отменен источник токена
                if (cts.IsCancellationRequested)
                {
                    //если отменен - разрушить и инициализировать новый
                    cts.Dispose();
                    return new CancellationTokenSource(); ;
                }
            }
            return cts;
        }


        /// <summary>
        /// Начало асинхнонного поиска Bluetooth устройств
        /// </summary>
        /// <param name="foundedPanelCallback">Обратный вызов при нахождении устройства</param>
        /// <param name="scanStartEndCallback">Обратный вызов при начале/конце сканирования</param>
        public async void StartSearchDeviceAsync(Action<DevPanel> foundedPanelCallback, Action<bool> scanStartEndCallback)
        {
            _foundedPanelCallback = foundedPanelCallback;
            
            try
            {
                //проверка того, что Bluetooth включен
                if (!await CheckBluetoothOn())
                {
                    return;
                }

            }
            catch (Exception)
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                        "Не удалось включить Bluetooth", "Ok");
                return;

            }

            //инициализация обработчика по событию обнаружения нового устройства
            EventHandler<DeviceEventArgs> devH = new EventHandler<DeviceEventArgs>((o, e) =>
            {
                //Проверка того, что устройство - BLE
                bool isBle = DependencyService.Get<IDepServices>().IsBleDevice(e.Device);
                

                if (foundedPanelCallback != null && isBle)       //если BLE - инициализировать новое устройство
                {
                    DevPanel newDev = GetPanel(e.Device);                    
                    if (newDev != null)
                    {
                        foundedPanelCallback(newDev);                //сделать обратный вызов
                    }                    
                }
            });


            IAdapter adapter = CrossBluetoothLE.Current.Adapter;
            adapter.ScanTimeout = BleScanTimeMs;          //установка времени сканирования
            adapter.DeviceDiscovered += devH;           //подписка на событие обнаружение нового устройства
            Сts = GetCts(Сts);                          //получить источника токена задачи сканирования            
            scanStartEndCallback?.Invoke(true);         //обратный вызов по началу задачи сканирования
                                                        //запуск сканирования
            
            await adapter.StartScanningForDevicesAsync(null, null, true, Сts.Token);
            await adapter.StopScanningForDevicesAsync();
            scanStartEndCallback?.Invoke(false);        //обратный вызов по завершению задачи сканирования            
            adapter.DeviceDiscovered -= devH;           //отписка от события обнаружения нового устройства BLE
            
        }


        /// <summary>
        /// Получение панели из обнаруженного Bluetooth устройства
        /// </summary>
        /// <param name="bleDev">Bluetooth устройство</param>
        /// <returns>Если найдена панель - возвращается объект панели, иначе - null</returns>
        private DevPanel GetPanel(IDevice bleDev)
        {
            try
            {
                //записи Advertise
                var adv = bleDev.AdvertisementRecords;
                //выборка из записей Advertise записи производителя согласно ее типа и длины
                var manufRecord = adv.SingleOrDefault(h =>
                h.Type == AdvertisementRecordType.ManufacturerSpecificData && h.Data.Length >= ManufacturerDataLen);
                //проверка что запись существует
                if (manufRecord == null)
                {                    
                    return null;
                }                  
                
                //получение байтов Major version
                byte[] recMajVer = new byte[MajorMinorVerSize];
                Buffer.BlockCopy(manufRecord.Data, MajorVerOffset, recMajVer, 0, recMajVer.Length);

                //получение байтов Minor version
                byte[] recMinorVer = new byte[MajorMinorVerSize];
                Buffer.BlockCopy(manufRecord.Data, MinorVerOffset, recMinorVer, 0, recMinorVer.Length);
                               
                //получение IBeacon UUID
                byte[] recBeaconId = new byte[UUID128Size];
                Buffer.BlockCopy(manufRecord.Data, IBeaconUUIDOffset, recBeaconId, 0, recBeaconId.Length);

                //байты имени стены
                byte[] wallNameBytes = new byte[WallNameLen];
                Buffer.BlockCopy(recBeaconId, 0, wallNameBytes, 0, wallNameBytes.Length);

                //4я часть идентификатора IBeacon из 4х байт
                byte[] idPart = new byte[UUID128Size - WallNameLen];
                Buffer.BlockCopy(recBeaconId, WallNameLen, idPart, 0, idPart.Length);

                //результирующий идентификатор из 4й части идентификатора IBeacon из 4х байт,
                //Major и minor версии
                byte[] resultId = idPart.Concat(recMajVer).Concat(recMinorVer).ToArray();
                //сравнение полученного идентификатора с заданным
                if (!resultId.SequenceEqual(DeviceID))
                {                   
                    return null;
                }
                //продолжение обработки, если идентификатор совпал
                //получение кодировки Windows-1251
                Encoding win1251 = Encoding.GetEncoding("Windows-1251");

                //получение имени стены
                string wallName = wallNameBytes.SequenceEqual(DefaultWallNameBytes) ? "-Без имени-" : win1251.GetString(wallNameBytes);
                wallName = $"Стена \"{wallName}\"";
                //убрать символы, представленные байтами 0x00,0x01
                wallName = wallName.Replace("\u0001", "");
                wallName = wallName.Replace("\u0000", "");

                //получение и проверка наличия записи первых 3,4го байта UUID сервиса UART
                var servUUIDRecord = adv.SingleOrDefault(h =>
                h.Type == AdvertisementRecordType.UuidsIncomple16Bit && h.Data.Length == UUID16Size);
                if (servUUIDRecord == null)
                {                    
                    return null;
                }

                //получение имени панели из Advertise данных
                var devNameAdv = adv.SingleOrDefault(h =>
                h.Type == AdvertisementRecordType.CompleteLocalName);

                string devName = Encoding.UTF8.GetString(devNameAdv.Data);
                devName = $"Панель \"{devName}\"";

                //получение типа панели из младшего байта 16-разрядной части UUID сервиса UART
                PanelType panelType;
                switch (servUUIDRecord.Data.Last())
                {
                    case 2:
                        panelType = PanelType.Simple;
                        break;
                    case 3:
                        panelType = PanelType.Telescopic;
                        break;
                    case 4:
                        panelType = PanelType.SimpleSmartGlass;
                        break;
                    case 5:
                        panelType = PanelType.TelescopicSmartGlass;
                        break;
                    default:
                        panelType = PanelType.Unsetted;
                        break;
                }
                //инициализация панели
                DevPanel panelDev = new DevPanel(Guid.NewGuid())
                {
                    Name = devName,
                    WallName = wallName,
                    PanType = panelType,
                    Instance = bleDev,
                    Rssi = bleDev.Rssi
                };
                return panelDev;
            }
            catch (Exception)
            {               
                return null;
            }         
            
        }


        /// <summary>
        /// Проверка включения Bluetooth
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CheckBluetoothOn()
        {

            IAdapter adapter = CrossBluetoothLE.Current.Adapter;                     //адаптер BLE
            if (adapter == null)                                            //проверка адаптера BLE
            {
                return false;
            }

            if (!CrossBluetoothLE.Current.IsOn)                             //если Bluetooth выключен
            {
                //запрос к пользователю на разрешение включения Bluettoth
                var answer = await App.Current.MainPage.DisplayAlert("Внимание",
                    "Включить Bluetooth для поиска устройств?", "Да", "Нет");
                //проверка подтверждения пользователем
                if (!answer)
                {
                    return false;
                }

                //источник токена на задачу ожидания включения Bluettoth
                CancellationTokenSource ctsWait = new CancellationTokenSource();
                //инициализация обработчика события изменения состояния адаптера Bluetooth
                EventHandler<BluetoothStateChangedArgs> btChangedEvh =
                    new EventHandler<BluetoothStateChangedArgs>((o, e) =>
                    {
                        //если Bluettoth включился - отменить токен ожидания включения Bluettoth
                        if (e.NewState == BluetoothState.On)
                        {
                            ctsWait.Cancel();
                        }
                    });
                //подписка на событие изменения состояния адаптера Bluetooth
                CrossBluetoothLE.Current.StateChanged += btChangedEvh;

                //Bluetooth (для каждой платформы - свое)
                await DependencyService.Get<IDepServices>().EnableBluetooth();
                int bluetoothOnWaitTimeout = 0;
                //установить таймаут ожидания вкл. Bluetooth в зависимости от ОС
                if (Device.RuntimePlatform == Device.iOS)
                {
                    //для iOS время включения больше, т.к. включает пользователь в настройках
                    bluetoothOnWaitTimeout = 30000;
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    bluetoothOnWaitTimeout = 6000;
                }

                //задача ожидания включения Bluetooth
                var timeoutTask = Task.Delay(bluetoothOnWaitTimeout, ctsWait.Token);

                await Task.WhenAny(timeoutTask);                            //Ожидание завершения задачи
                ctsWait.Dispose();                                          //разрушение источника токена задачи ожидания
                if (!timeoutTask.IsCanceled)                                //если задача не была отменена, то считать, что был таймаут - вывести сообщение
                {
                    await App.Current.MainPage.DisplayAlert("Внимание",
                    "Не удалось включить Bluetooth.", "Ok");
                    return false;
                }
                //отписка от события изменения состояния адаптера Bluetooth
                CrossBluetoothLE.Current.StateChanged -= btChangedEvh;

            }
            return true;
        }
    }
}
