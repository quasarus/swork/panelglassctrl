﻿using Plugin.BLE.Abstractions.Contracts;
using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SHControl.Services
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public interface IBluetoothConnector
    {
        Task<bool> ConnectToDeviceAsync(DevPanel device);
        Task<string> SendCommandAsync(string command, bool isAtPacket);        

        DevPanel GetConnectedPanel();

        Task DisconnectDeviceAsync();

        bool IsDeviceConnected();
        void Dispose();
                
    }
}
