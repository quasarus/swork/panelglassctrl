﻿using GalaSoft.MvvmLight.Command;
using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Linq;
using System.ComponentModel;
using GalaSoft.MvvmLight.Messaging;
using SHControl.Services;
using Xamarin.Forms;
using SHControl.Views;
using System.Threading.Tasks;
using SHControl.Messages;
using GalaSoft.MvvmLight.Ioc;
using static Android.Renderscripts.ScriptGroup;
using Binding = Xamarin.Forms.Binding;

namespace SHControl.ViewModels
{
    /// <summary>
    /// Модель-представление списка стен
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class WallsPageVm : BaseViewModel
    {
        /// <summary>
        /// Команда обновления списка стен и панелей
        /// </summary>
        public ICommand RefreshCommand { get; set; }
        /// <summary>
        /// Команда перехода на следующую страницу после выбора панелей
        /// </summary>
        public ICommand NextPageCommand { get; set; }
        /// <summary>
        /// Команда появления страницы
        /// </summary>
        public ICommand AppearingCommand { get; set; }
        /// <summary>
        /// Команда исчезновения страницы
        /// </summary>
        public ICommand DisappearingCommand { get; set; }

        private IBToothSearcher _btSercher;
        private IUserDataStore _userDataStore;
        private IBluetoothConnector _btConnector;        


        private bool _isScanDevs;
        /// <summary>
        /// Флаг того, что идет сканирование на новые устройства Bluetooth
        /// </summary>

        public bool IsScanDevs
        {
            get { return _isScanDevs; }
            set
            {
                SetProperty(ref _isScanDevs, value);
                //выставление текста кнопки в зависимости от состояния поиска новых устройств Bluetooth
                if (!IsBusy)
                {
                    SearchButtonText = value ? "Отменить" : "Поиск устройств";
                }
                else
                {
                    SearchButtonText = "";
                }
                                
            }
        }

        private AppModeType _appMode;
        /// <summary>
        /// Режим работы приложения
        /// </summary>
        public AppModeType AppMode
        {
            get { return _appMode; }
            set
            {                
                SetProperty(ref _appMode, value);                
            }
        }



        private string _searchButtonText;
        /// <summary>
        /// Текст кнопки начала\завершения сканирования новых устройств Bluetooth
        /// </summary>
        public string SearchButtonText
        {
            get { return _searchButtonText; }
            set
            {
                SetProperty(ref _searchButtonText, value);
            }
        }

        private bool _isSomeSelected;
        /// <summary>
        /// Флаг того, что выбрана хоть одна панель для дальнейших операций
        /// </summary>
        public bool IsSomeSelected
        {
            get { return _isSomeSelected; }
            set
            {
                SetProperty(ref _isSomeSelected, value);
            }
        }

        private string _busyMessage;
        /// <summary>
        /// Сообщение, выводимое при длительной операции
        /// </summary>

        public string BusyMessage
        {
            get { return _busyMessage; }
            set
            {
                SetProperty(ref _busyMessage, value);
            }
        }

        private ObservableCollection<WallGroup> _wallss;
        /// <summary>
        /// Список стен с панелями
        /// </summary>
        public ObservableCollection<WallGroup> Walls
        {
            get { return _wallss; }
            set
            {
                SetProperty(ref _wallss, value);
            }
        }                  

        public WallsPageVm(IBToothSearcher btSercher, IUserDataStore userDataStore, IBluetoothConnector btConnector)
        {
            _btConnector = btConnector;
            _btSercher = btSercher;
            _userDataStore = userDataStore;
            
            NextPageCommand = new RelayCommand(OnNextPageCommand);
            AppearingCommand = new RelayCommand(OnAppearingCommand);
            DisappearingCommand = new RelayCommand(OnDisappearingCommand);
            RefreshCommand = new RelayCommand(OnRefreshCommand);
            
            IsScanDevs = false;
            Walls = new ObservableCollection<WallGroup>();            
            Title = "Список стен";
            Messenger.Default.Register<NotificationMessage<bool>>(this, (result) =>
            {
                //регистрация на сообщение выбора панелей чекбоксом в технологическом режиме и установка флага, что что-то выбрано
                var selectedPans = Walls.SelectMany(v => v.Panels).Where(h => h.IsSelectedByCheckBox);
                if (selectedPans.Count() > 0)
                {
                    IsSomeSelected = AppMode == AppModeType.Tech ? true : false;
                }
                else
                {
                    IsSomeSelected = false;
                }
                

            });            

            Messenger.Default.Register<UserPanelSelectMessage>(this,async (mess) =>
            {
                // регистрация на сообщение выбора панели в пользовательском режиме
                var dataStorage = SimpleIoc.Default.GetInstance<IUserDataStore>();
                dataStorage.SetSelectedPanels(new List<DevPanel>() { mess.Panel });
                try
                {
                    HandlePanelSelection(dataStorage.GetSelectedPanels());
                }
                catch (Exception ex)
                {
                    await App.Current.MainPage.DisplayAlert("Ошибка",
                                                    ex.Message, "Ok");
                }
                
            });

        }

        /// <summary>
        /// Останов поиска Bluetooth устройств
        /// </summary>
        private void StopSearch()
        {
            var searcher = SimpleIoc.Default.GetInstance<IBToothSearcher>();
            searcher.CancelCurrentOperation();
            SearchButtonText = "";
        }

        /// <summary>
        /// Обработка выбора панелей и переход к командам
        /// </summary>
        /// <param name="selPanels"></param>
        private async void HandlePanelSelection(List<DevPanel> selPanels)
        {
            //проверка, что список выбранных панелей существует
            if (selPanels == null)
            {
                throw new Exception("Ошибка при выборе панелей.");
            }
            //проверка, что выбрана только одна панель, обработка нескольких панелей не реализована
            if (selPanels.Count > 1)
            {
                await App.Current.MainPage.DisplayAlert("Внимание",
                                                    $"Настройка нескольких панелей сразу еще не поддерживается." +
                                                    $" Выберите одну панель для настройки.", "Ok");
                return;
            }
            
            //если в пользовательском режиме выбрана 1 панель
            if (selPanels.Count == 1 || AppMode == AppModeType.User)
            {
                DevPanel panel = selPanels.ElementAt(0);
                BusyMessage = $"Сопряжение с панелью {panel.Instance.Name}";
                IsBusy = true;                                              //выставить флаг длительной операции
                StopSearch();                                               //остановить поиск новых Bluetooth устройств        
                                
                await _btConnector.ConnectToDeviceAsync(panel);             //сопряжение с панелью
                
                IsBusy = false;                                             //снятие флага длительной операции
                //перейти на страницу команд
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new ConfigSelectPage()));
                
            }            
        }

        

        private void OnDisappearingCommand()
        {
            
        }

        /// <summary>
        /// При появлении страницы
        /// </summary>
        private async void OnAppearingCommand()
        {            
            await _btConnector.DisconnectDeviceAsync();             //выполнить разрыв сопряжения
            IsScanDevs = false;                                     //снять флаг операции поиска новых устройств Bluetooth
            Walls = new ObservableCollection<WallGroup>();
            AppMode = _userDataStore.GetAppMode();                  //обновить выбранный режим работы приложения            
        }


        /// <summary>
        /// По переходу в технологическом режиме к странице команд
        /// </summary>
        private async void OnNextPageCommand()
        {
            try
            {
                //получение списка выбранных панелей
                var selectedPanels = GetSelectedPanelsList(Walls.ToList());
                //сохранение списка выбранных панелей в хранилище
                _userDataStore.SetSelectedPanels(selectedPanels);
                //обработка выбора панелей
                HandlePanelSelection(_userDataStore.GetSelectedPanels());
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Ошибка",
                                                    ex.Message, "Ok");
            }                     
            
        }


        /// <summary>
        /// Получение списка выбранных панелей
        /// </summary>
        /// <param name="walls">Список стен</param>
        /// <returns></returns>
        private List<DevPanel> GetSelectedPanelsList(List<WallGroup> walls)
        {
            //получение списка панелей, у которых выставлен флаг выбора чекбоксом
            var selectedPans = Walls.SelectMany(v => v.Panels).Where(h => h.IsSelectedByCheckBox);
            if (selectedPans == null)
            {
                throw new Exception("Не выбрана панель");
            }
            return selectedPans.ToList();
        }

        /// <summary>
        /// Очистка списка стен
        /// </summary>
        private void ClearAllWalls()
        {
            //сначала найти выбранные чекбоксом панели и снять чекбоксы (для снятия флага выбора хотя бы одной панели)
            var selectedPans = Walls.SelectMany(v => v.Panels).Where(h => h.IsSelectedByCheckBox);
            
            foreach (var item in selectedPans)
            {
                item.IsSelectedByCheckBox = false;
            }
            //очистка списка стен
            Walls.Clear();           
            
        }

        /// <summary>
        /// По команде обновления списка найденных панелей
        /// </summary>
        private void OnRefreshCommand()
        {
            //если уже не идет длительная операция - вернуться
                if (IsBusy)
                {
                    return;
                }
                //если уже идет сканирование - отменить операцию сканирования
                if (IsScanDevs)
                {
                    _btSercher.CancelCurrentOperation();
                    return;
                }
                _btSercher.CancelCurrentOperation();
                ClearAllWalls();
                IsScanDevs = true;
                //запустить сканирование Bluetooth
                _btSercher.StartSearchDeviceAsync((newPan) =>
                {   
                    //при обнаружении новой панели
                    OnNewPanelFound(newPan);
                },
                (isScan) =>
                {
                    //установка флага сканирования по колбэку
                    IsScanDevs = isScan;
                });
            
            

        }

        /// <summary>
        /// При обружении новой панели
        /// </summary>
        /// <param name="newPan">Новая панель</param>
        private void OnNewPanelFound(DevPanel newPan)
        { 
            //проверить имя стены новой панел, если такая стена уже существует, то добавить в нее панель,
            //иначе - создать стену и добавить в нее панель
            var sameWall = Walls.SingleOrDefault(d => d.Name == newPan.WallName);
            if (sameWall == null)
            {
                var newWall = new WallGroup(Guid.NewGuid())
                {
                    Name = newPan.WallName
                };
                
                newWall.AddPanel(newPan);                
                Walls.Add(newWall);
                
            }
            else
            {                
                sameWall.AddPanel(newPan);                
            }
        }

        /// <summary>
        /// При нажатии кнопки Назад на странице
        /// </summary>
        public async void OnBackButton()
        {
            //остановить поиск новых устройств Bluetooth
            StopSearch();
            //разорвать сопряжение с панелью, если есть
            await _btConnector.DisconnectDeviceAsync();
            //перейти на главную страницу приложения
            await App.Current.MainPage.Navigation.PushModalAsync(new MainPage());
        }

        /// <summary>
        /// При исчезновении страницы
        /// </summary>
        public void OnDisappearing()
        {
            //очистить список стен
            ClearAllWalls();
        }
    }
}
