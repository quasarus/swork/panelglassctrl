﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using SHControl.DependencyServices;
using SHControl.Messages;
using SHControl.Models;
using SHControl.Services;
using SHControl.Views;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace SHControl.ViewModels
{
    /// <summary>
    /// Модель представления для страницы авторизации и и страницы пользователей
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class MainPageVm:BaseViewModel
    {
        private IUserDataStore _dataStore;
        //команды появления и исчезновения страницы авторизации
        public ICommand AppearingCommand { get; set; }
        public ICommand DisappearingCommand { get; set; }
        //команда появления страницы пользователей
        public ICommand UserPageAppearingCommand { get; set; }
        //команда добавления пользователя
        public ICommand AddUserCommand { get; set; }
        //команда старта авторизации
        public ICommand StartAuthCommand { get; set; }
        //команда выбора пользовательского режима
        public ICommand UserModeCommand { get; set; }
        //команда выбора технологического режима
        public ICommand TechModeCommand { get; set; }
        //команда подтверждения завершения ввода данных нового пользователя
        public ICommand UserConfirmCommand { get; set; }
               

        private AuthStatusType _authStatus;
        /// <summary>
        /// Состояние авторизации
        /// </summary>
        public AuthStatusType AuthStatus
        {
            get { return _authStatus; }
            set
            {
                SetProperty(ref _authStatus, value);                

                switch (value)
                {                    
                    case AuthStatusType.AuthAsAdmin:
                        //если зашел как админ - отобразить в меню пункты выхода и списка пользователей
                        AddMenuItem(MenuItemType.ManageUser, "Пользователи");
                        AddMenuItem(MenuItemType.LogOut, "Выход");                        
                        break;
                    case AuthStatusType.AuthAsUser:
                        //если зашел как пользователь - отобразить в меню пункт выхода
                        AddMenuItem(MenuItemType.LogOut, "Выход");                                                                  
                        break;
                    default:

                        IsPresented = false;                        //скрытие меню
                        //удаление из меню пунктов выхода и списка пользователей
                        RemoveMenuItem(MenuItemType.LogOut);        
                        RemoveMenuItem(MenuItemType.ManageUser);
                        break;
                }

                //установить заголовки согласно текущему состоянию авторизации
                if (value == AuthStatusType.BeforeProcessOfAuth || value == AuthStatusType.InProcessOfAuth
                    || value == AuthStatusType.NonAuthorized)
                {
                    Title = "Авторизация";
                }
                else
                {
                    Title = $"{_dataStore.GetUserDescription()}";
                }                
                
            }
        }

        /// <summary>
        /// Удаление пункта меню
        /// </summary>
        /// <param name="type">Тип пункта меню</param>
        private void RemoveMenuItem(MenuItemType type)
        {
            HomeMenuItems.Remove(new HomeMenuItem() { Id = type });        
        }


        /// <summary>
        /// добавление пункта меню
        /// </summary>
        /// <param name="type">Тип пункта меню</param>
        /// <param name="title">Заголовок пункта меню</param>
        private void AddMenuItem(MenuItemType type, string title)
        {
            if (!HomeMenuItems.Any(f=>f.Id == type))
            {
                HomeMenuItems.Add(new HomeMenuItem { Id = type, Title = title });
            }    
            
        }

        private ObservableCollection<HomeMenuItem> _menuItems;
        /// <summary>
        /// Элементы меню основной страницы
        /// </summary>
        public ObservableCollection<HomeMenuItem> HomeMenuItems
        {
            get { return _menuItems; }
            set
            {                
                SetProperty(ref _menuItems, value);
            }
        }


        private HomeMenuItem _selMenuItem;
        /// <summary>
        /// Выбранный элемент меню основной страницы
        /// </summary>
        public HomeMenuItem SelectedMenuItem
        {
            get { return _selMenuItem; }
            set
            {
                SetProperty(ref _selMenuItem, value);
                if (SelectedMenuItem != null)
                {
                    //обработка выбора меню
                    HandleSelectedHomeMenuItem(value);
                    SelectedMenuItem = null;
                }                
            }
        }

        private ObservableCollection<UserAuthData> _userList;
        /// <summary>
        /// Список пользователей
        /// </summary>
        public ObservableCollection<UserAuthData> UserList
        {
            get { return _userList; }
            set
            {                
                SetProperty(ref _userList, value);
            }
        }

        private bool _isUserAdding;
        /// <summary>
        /// Флаг того, что выбрано добавление пользователя
        /// </summary>
        public bool IsUserAdding
        {
            get { return _isUserAdding; }
            set
            {
                Title = value ? "Добавление пользователя" : "Список пользователей";
                //очистка полей ввода при появлении элементов GUI для добавления нового пользователя
                if (value)
                {
                    NewUserLogin = NewUserPassword = NewUserPasswordConfirm = "";                    
                }                
                SetProperty(ref _isUserAdding, value);

            }
        }


        /// <summary>
        /// Обработка выбора пункта меню основной страницы
        /// </summary>
        /// <param name="item"></param>
        private async void HandleSelectedHomeMenuItem(HomeMenuItem item)
        {
            switch (item.Id)
            {                
                case MenuItemType.LogOut:
                    //если логаут - сбросить автологин
                    _dataStore.SetAutoAuthorize(false);
                    //обнулить имя и пароль пользователя и перейти в состояние - до начала авторизации
                    IsAutoAuth = false;
                    Password = "";
                    UserName = "";
                    AuthStatus = AuthStatusType.BeforeProcessOfAuth;
                    break;
                case MenuItemType.ManageUser:
                    //если список пользователей - создать список, перейти на страницу списка пользователей
                    UserList = new ObservableCollection<UserAuthData>(await _dataStore.GetUsersAsync());
                    await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new AddUserPage()));
                    break;
                default:
                    break;
            }
            
        }        


        private bool isPresented;
        /// <summary>
        /// Флаг отображения меню основной страницы раскрытым
        /// </summary>
        public bool IsPresented
        {
            get { return isPresented; }
            set
            {
                SetProperty(ref isPresented, value);
            }
        }


        private bool _isAutoAuth;
        /// <summary>
        /// Флаг того, выставлен ли пользователем автологин
        /// </summary>
        public bool IsAutoAuth
        {
            get { return _isAutoAuth; }
            set
            {                
                _dataStore.SetAutoAuthorize(value);
                SetProperty(ref _isAutoAuth, value);
            }
        }

        private string _userName;
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set
            {
                SetProperty(ref _userName, value);
            }
        }

        /// <summary>
        /// Пароль
        /// </summary>
        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {                
                SetProperty(ref _password, value);                
            }
        }


        private string _newUserName;
        /// <summary>
        /// Логин нового пользователя
        /// </summary>
        public string NewUserLogin
        {
            get { return _newUserName; }
            set
            {
                SetProperty(ref _newUserName, value);
            }
        }

        private string _newUserPassword;
        /// <summary>
        /// Пароль нового пользователя
        /// </summary>
        public string NewUserPassword
        {
            get { return _newUserPassword; }
            set
            {                
                SetProperty(ref _newUserPassword, value);
            }
        }

        private string _newUserPasswordConfirm;
        /// <summary>
        /// Подтверждение пароля нового пользователя
        /// </summary>
        public string NewUserPasswordConfirm
        {
            get { return _newUserPasswordConfirm; }
            set
            {
                SetProperty(ref _newUserPasswordConfirm, value);
            }
        }

        public MainPageVm(IUserDataStore dataStore)
        {
            _dataStore = dataStore;
            UserList = new ObservableCollection<UserAuthData>();
            AppearingCommand = new RelayCommand(OnPageAppearing);
            DisappearingCommand = new RelayCommand(OnPageDisappearing);
            StartAuthCommand = new RelayCommand(OnStartAuthorization);
            UserPageAppearingCommand = new RelayCommand(()=> { IsUserAdding = false; });
            UserConfirmCommand=new RelayCommand(OnUserConfirmCommand);
            UserModeCommand = new RelayCommand(async ()=> 
            {
                //при выборе пользовательского режима сохранить режим в хранилище и перейти на страницу списка стен
                _dataStore.SetAppMode(AppModeType.User);
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()));
            });
            TechModeCommand = new RelayCommand(async () =>
            {
                //при выборе технологического режима сохранить режим в хранилище и перейти на страницу списка стен
                _dataStore.SetAppMode(AppModeType.Tech);
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()));
            });
            AddUserCommand = new RelayCommand(() => 
            {
                //при нажатии кнопки добавления нового пользователя - установить флаг добавления, откроется часть страницы с вводом данных
                IsUserAdding = true;
            });
            Messenger.Default.Register<AuthUserDeleteMessage>(this,async (mess) =>
            {
                //по приходу сообщения удаления пользователей - удалить польщователя
                await _dataStore.RemoveUserAsync(mess.Login);
                //обновить список пользователей
                UserList = new ObservableCollection<UserAuthData>(await _dataStore.GetUsersAsync());
                //проверить что пользователь был удален
                if (UserList.Select(g => g.Login).Any(n => n == mess.Login))
                {
                    await App.Current.MainPage.DisplayAlert("Ошибка",
                                                    "Не удалось удалить пользователя.", "Ok");
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Уведомление",
                                                    "Пользователь успешно удален.", "Ok");
                }
            });

            HomeMenuItems = new ObservableCollection<HomeMenuItem>();
            //получить номер версии и отображать ее в элементе меню
            string version = DependencyService.Get<IDepServices>().GetVersion();
            HomeMenuItems.Add(new HomeMenuItem { Id = MenuItemType.Version, Title = $"Версия {version}" });
            
            AuthStatus = AuthStatusType.BeforeProcessOfAuth;
            _dataStore.InitAuthData();            
        }

        /// <summary>
        /// По нажатию кнопки добавления пользователя
        /// </summary>
        private async void OnUserConfirmCommand()
        {
            try
            {
                //проверка что пользователя с таким логином еще нет
                if (UserList.Select(f => f.Login).Any(b => b == NewUserLogin))
                {
                    await App.Current.MainPage.DisplayAlert("Ошибка",$"Пользователь с именем {NewUserLogin} уже существует.", "Ok");
                    return;
                }

                //проверка совпадения пароля и его подтверждения
                if (NewUserPassword != NewUserPasswordConfirm)
                {
                    await App.Current.MainPage.DisplayAlert("Ошибка", "Пароль и подтверждение пароля не совпадают.", "Ok");
                    return;
                }
                //добавление пользователя в хранилище настроек
                UserAuthData newUser = new UserAuthData()
                {
                    Description = "Пользователь",
                    IsAdmin = false,
                };
                await _dataStore.AddNewUserAsync(newUser, NewUserLogin, NewUserPassword);
                //обновление списка пользователей
                IsUserAdding = false;
                UserList = new ObservableCollection<UserAuthData>(await _dataStore.GetUsersAsync());
                //проверка, что пользователь добавлен
                if (UserList.Select(f => f.Login).Any(b => b == NewUserLogin))
                {
                    await App.Current.MainPage.DisplayAlert("Уведомление", "Пользователь успешно добавлен.", "Ok");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                await App.Current.MainPage.DisplayAlert("Ошибка", "Не удалось добавить пользователя.", "Ok");
            }
            
        }

        /// <summary>
        /// При старте авторизации
        /// </summary>
        private async void OnStartAuthorization()
        {
            //поменять статус процесса авторизации
            AuthStatus = AuthStatusType.InProcessOfAuth;   
            //запустить авторизацию
            AuthStatus = await _dataStore.Authorise(UserName, Password, IsAutoAuth);
            //если авторизован как пользователь, то сразу перейти на страницу с списком стен
            if (AuthStatus == AuthStatusType.AuthAsUser)
            {
                _dataStore.SetAppMode(AppModeType.User);
                await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()));
            }
        }

        private void OnPageDisappearing()
        {
            
        }

        /// <summary>
        /// При появлении страницы
        /// </summary>
        private async void OnPageAppearing()
        {
            //если еще не авторизовался, запустить автологин
            if (AuthStatus != AuthStatusType.AuthAsAdmin && AuthStatus != AuthStatusType.AuthAsUser)
            {
                AuthStatus = AuthStatusType.InProcessOfAuth;
                try
                {                    
                    IsAutoAuth = _dataStore.IsAutoAuthorize();
                }
                catch (Exception)
                {
                    await App.Current.MainPage.DisplayAlert("Внимание",
                            "Не удалось инициализировать хранилище данных приложения", "Ok");
                    return;
                }

                AuthStatusType result = await _dataStore.AutoAuthoriseAsync();
                AuthStatus = result == AuthStatusType.NonAuthorized ?
                    AuthStatusType.BeforeProcessOfAuth : result;
            }
            else
            {
                //повторно установить состояние авторизации, чтобы корректно обновить заголовок страницы
                AuthStatus = AuthStatus;
            }

        }

        public void OnDisappearing()
        {
            IsUserAdding = false;
        }
    }
}
