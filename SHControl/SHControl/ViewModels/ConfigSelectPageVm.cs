﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using SHControl.Messages;
using SHControl.Models;
using SHControl.Services;
using SHControl.Views;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using SHControl.DependencyServices;

namespace SHControl.ViewModels
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public class ConfigSelectPageVm : BaseViewModel
    {

        private IUserDataStore _userDataStore;

        private IBluetoothConnector _btConnector;


        private string _busyMessage;
        /// <summary>
        /// Сообщение в момент выполнения длительного действия
        /// </summary>

        public string BusyMessage
        {
            get { return _busyMessage; }
            set
            {
                SetProperty(ref _busyMessage, value);
            }
        }

        /// <summary>
        /// Комманды
        /// </summary>
        private List<DevCommand> _devCommands;
        public List<DevCommand> DevCommands
        {
            get { return _devCommands; }
            set
            {
                SetProperty(ref _devCommands, value);
            }
        }

        /// <summary>
        /// Команда появления страницы
        /// </summary>
        public ICommand AppearingCommand { get; set; }
        /// <summary>
        /// Команда исчезновения страницы
        /// </summary>
        public ICommand DisappearingCommand { get; set; }
        //public ICommand AppExitCommand { get; set; }

        public ConfigSelectPageVm(IUserDataStore userDataStore, IBluetoothConnector btConnector)
        {
            _btConnector = btConnector;
            _userDataStore = userDataStore;
            AppearingCommand = new RelayCommand(OnAppearingCommand);
            DisappearingCommand = new RelayCommand(OnDisappearingCommand);

            //при получении сообщения о занятости установить флаг и сообщение
            Messenger.Default.Register<BusyMessage>(this, (mess) =>
            {
                IsBusy = mess.IsBusy;
                BusyMessage = mess.Message;
            });
            Title = "Команды";
        }

        /// <summary>
        /// создать команды для пользователя
        /// </summary>
        /// <returns></returns>
        private List<DevCommand> CreateUserCommands()
        {
            //получить выбранные панели
            var dataStorage = SimpleIoc.Default.GetInstance<IUserDataStore>();
            DevPanel panel = dataStorage.GetSelectedPanels().FirstOrDefault();

            List<DevCommand> commands = new List<DevCommand>();

            //если панли без стекла, создать команду только об убирании панели, 
            //если есть стекло, команду о затемнении
            if (panel.PanType == PanelType.Simple
                || panel.PanType == PanelType.Telescopic)
            {
                commands.Add(new SimpleCommand()
                {
                    Description = "Убрать панель",
                    ExecText = "Выполнить",
                    ComString = "AC"
                });
            }
            else if (panel.PanType == PanelType.SimpleSmartGlass
                || panel.PanType == PanelType.TelescopicSmartGlass)
            {
                commands.Add(new SimpleCommand()
                {
                    Description = "Убрать панель",
                    ExecText = "Выполнить",
                    ComString = "AC"
                });
                commands.Add(new SimpleCommandSlider("", 255, 0)
                {
                    Description = "Затемнение смарт-стекла",
                    ExecText = "Выполнить",
                    ComString = "GL",
                    CommandParamDigitsNum = 3,                    
                });

            }
            else
            {
                //если тип панели еще не задан - отобразить ошибку
                throw new Exception("Выбранная панель имеет неизвестный тип. Установите тип панели.");
            }
            return commands;
        }


        /// <summary>
        /// TODO Создать команды, если выбрано несколько панелей в технологическом режиме 
        /// </summary>
        /// <returns></returns>
        private List<DevCommand> CreateTechCommandsMultiple()
        {
            List<DevCommand> commands = new List<DevCommand>();

            #region Инициализация команд
            commands.Add(new SimpleCommand()
            {
                Description = "Убрать панель",
                ExecText = "Выполнить",
                ComString = "AC"
            });
            commands.Add(new SetTimeCommand()
            {
                Description = "Установить текущее время",
                ExecText = "Выполнить",
                ComString = "ST"
            });

            const string driverTypeOne = "VNH2SP30", driverTypeTwo = "VNH3ASP30";

            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить тип драйвера",
                SubCommands = new List<DevCommand>()
                {
                        new SetDriverTypeCommand()
                        {
                            Description = "Верхний актуатор", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {

                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "DT"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Нижний актуатор", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "DB"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Боковой актуатор 1", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "D1"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Боковой актуатор 2", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "D2"
                        }
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить тип панели",
                SubCommands = new List<DevCommand>()
                {
                    new SetPanelTypeCommand()
                        {
                            Description = "Тип панели", ExecText="Задать", ComString="PN",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name="Простая",Id = 1,
                                    AddString = "S", AddStringPacket="AT+UUID0x0002" },
                                new PickerData(){Name="Телескопическая",Id = 2,
                                    AddString = "T", AddStringPacket="AT+UUID0x0003" },
                                new PickerData(){Name="Простая со смарт-стеклом",Id = 3,
                                    AddString = "S", AddStringPacket="AT+UUID0x0004" },
                                new PickerData(){Name="Телескопическая со смарт-стеклом",Id = 4,
                                    AddString = "T", AddStringPacket="AT+UUID0x0005" },
                            }
                        }
                }
            });

            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить название стены устройства",
                SubCommands = new List<DevCommand>()
                    {
                    new SetPanelWallNameCommand(){Description = $"Название стены", ExecText = "Задать",
                        MaxLength = 12}
                    }
            });
            #endregion            

            return commands;
        }

        /// <summary>
        /// Создать команды, если выбрана одна панель
        /// </summary>
        /// <returns></returns>
        private List<DevCommand> CreateTechCommandsSingle()
        {

            List<DevCommand> commands = new List<DevCommand>();

            #region Инициализация команд
            const string driverTypeOne = "VNH2SP30", driverTypeTwo = "VNH3ASP30";
            commands.Add(new SimpleCommand()
            {
                Description = "Убрать панель",
                ExecText = "Выполнить",
                ComString = "AC"
            });
            commands.Add(new SetTimeCommand()
            {
                Description = "Установить текущее время",
                ExecText = "Выполнить",
                ComString = "ST"
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Затемнение смарт-стекла",
                SubCommands = new List<DevCommand>()
                    {
                        new SimpleCommandSlider("",255,0)
                    {
                    Description = "Затемнение стекла",
                    ExecText = "Задать",
                    ComString = "GL",
                    CommandParamDigitsNum = 3,                    
                    }
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Установки для всех актуаторов",
                SubCommands = new List<DevCommand>()
                    {
                     new SimpleCommandSlider("кгс",92,1){Description = $"Максимальное усилие", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="FA", CommandParamDigitsNum = 2},
                     new SetDriverTypeCommand()
                        {
                            Description = "Тип драйвера", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {

                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "DA"
                        }
                    }
            });


            commands.Add(new SimpleCommandSub()
            {
                Description = "Получить историю",
                ExecText = "Выполнить",
                ComString = "GH",
                SubCommands = new List<DevCommand>()
                    {
                        new GetHistoryCommand(){Description = $"Число действий", ExecText = "Вывести",
                        MaxLength = 3, ComString = "GH"}
                    }
            });


            commands.Add(new SimpleCommandSub()
            {
                Description = "Задвижение актуаторов",
                SubCommands = new List<DevCommand>()
                {
                    new SimpleCommand(){Description = "Верхний актуатор", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="TC"},
                    new SimpleCommand(){Description = "Нижний актуатор", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="BC"},
                    new SimpleCommand(){Description = "Боковые актуаторы", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="SC"},                    
                    new SimpleCommand(){Description = "Все актуаторы", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="AC"},
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Выдвижение актуаторов",
                SubCommands = new List<DevCommand>()
                {
                    new SimpleCommand(){Description = "Верхний актуатор", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="TO"},
                    new SimpleCommand(){Description = "Нижний актуатор", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="BO"},                    
                    new SimpleCommand(){Description = "Боковые актуаторы", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="SO"},
                    new SimpleCommand(){Description = "Все актуаторы", ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="AO"},
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить максимальное усилие",
                SubCommands = new List<DevCommand>()
                {
                    new SimpleCommandSlider("кгс",92,1){
                        Description = $"Верхний актуатор",
                        ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="FT",
                        CommandParamDigitsNum = 2,                        
                        FetchCommand = new GetFetchCommand()
                        {                            
                            ComString= "FT",
                            Description = "запрос максимального усилия верхнего актуатора"
                        }},
                    new SimpleCommandSlider("кгс",92,1){Description = $"Нижний актуатор",
                        ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="FB",
                        CommandParamDigitsNum = 2,                        
                        FetchCommand = new GetFetchCommand()
                        {                            
                            ComString= "FB",
                            Description = "запрос максимального усилия нижнего актуатора"
                        }},
                    new SimpleCommandSlider("кгс",92,1){Description = $"Боковой актуатор 1",
                        ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="F1",
                        CommandParamDigitsNum = 2,
                        
                        FetchCommand = new GetFetchCommand()
                        {                            
                            ComString= "F1",
                            Description = "запрос максимального усилия бокового актуатора 1"
                        }},
                    new SimpleCommandSlider("кгс",92,1){Description = $"Боковой актуатор 2",
                        ExecText = "Выполнить",
                        IsLeftOffset = true, ComString="F2",
                        CommandParamDigitsNum = 2,                        
                        FetchCommand = new GetFetchCommand()
                        {                            
                            ComString= "F2",
                            Description = "запрос максимального усилия бокового актуатора 2"
                        }}
                }
            });



            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить тип драйвера",
                SubCommands = new List<DevCommand>()
                {
                        new SetDriverTypeCommand()
                        {
                            Description = "Верхний актуатор", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {

                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "DT"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Нижний актуатор", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "DB"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Боковой актуатор 1", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "D1"
                        },
                        new SetDriverTypeCommand()
                        {
                            Description = "Боковой актуатор 2", ExecText="Задать",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name=driverTypeOne,Id = 1, AddString = "0" },
                                new PickerData(){Name=driverTypeTwo,Id = 2, AddString = "1" },
                            },
                            ComString = "D2"
                        }
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить тип панели",
                SubCommands = new List<DevCommand>()
                {
                    new SetPanelTypeCommand()
                        {
                            Description = "Тип панели", ExecText="Задать", ComString="PN",
                            PickerItems=new ObservableCollection<PickerData>()
                            {
                                new PickerData(){Name="Простая",Id = 1,
                                    AddString = "S", AddStringPacket="AT+UUID0x0002" },
                                new PickerData(){Name="Телескопическая",Id = 2,
                                    AddString = "T", AddStringPacket="AT+UUID0x0003" },
                                new PickerData(){Name="Простая со смарт-стеклом",Id = 3,
                                    AddString = "S", AddStringPacket="AT+UUID0x0004" },
                                new PickerData(){Name="Телескопическая со смарт-стеклом",Id = 4,
                                    AddString = "T", AddStringPacket="AT+UUID0x0005" },
                            }
                        }
                }
            });

            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить название панели",
                SubCommands = new List<DevCommand>()
                {
                    new SetPanelNameCommand(){Description = $"Название панели", ExecText = "Задать",
                        MaxLength = 11, ComString = "AT+NAME" }
                }
            });
            commands.Add(new SimpleCommandSub()
            {
                Description = "Установить название стены устройства",
                SubCommands = new List<DevCommand>()
                    {
                    new SetPanelWallNameCommand(){Description = $"Название стены", ExecText = "Задать",
                        MaxLength = 12}
                    }
            });

            commands.Add(new GetVersionCommand()
            {
                Description = "Версия ПО Arduino",
                ExecText = "Получить",
                ComString = "VR"
            });
            //GetStoredParameters(commands);
            
            #endregion

            return commands;
        }

        /*private void GetStoredParameters(List<DevCommand> commands)
        {
            foreach (var command in commands)
            {
                command.GetStoredParameters();
            }
        }*/


        /// <summary>
        /// По исчезновению окна команд очистить команды
        /// </summary>
        public void OnDisapearing()
        {

            DevCommands?.Clear();
            DevCommands = null;
        }

        private void OnDisappearingCommand()
        {

        }

        /// <summary>
        /// По появлению окна команд
        /// </summary>
        private async void OnAppearingCommand()
        {
            //если технологический режим...
            if (_userDataStore.GetAppMode() == AppModeType.Tech)
            {
                //если выбрана 1 панель...
                if (_userDataStore.GetSelectedPanels().Count == 1)
                {
                    DevCommands = CreateTechCommandsSingle();
                }
                else
                {
                    DevCommands = CreateTechCommandsMultiple();
                }
            }
            else   //если пользовательский режим
            {
                try
                {
                    DevCommands = CreateUserCommands();
                }
                catch (Exception ex)
                {
                    await App.Current.MainPage.DisplayAlert("Ошибка",
                                                   ex.Message, "Ok");
                    await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
                }

            }
        }
    }
}
