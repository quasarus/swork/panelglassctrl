﻿using GalaSoft.MvvmLight.Ioc;
using SHControl.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace SHControl.ViewModels
{
    /// <summary>
    /// Локатор моделей-представления
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class ViewModelLocator
    {
        
        static ViewModelLocator()
        {      
            //регистрация сервисов, хранилищ, моделей-представления
                SimpleIoc.Default.Register<IUserDataStore, UserDataStore>();
                SimpleIoc.Default.Register<IBToothSearcher, BToothSearcher>();
                SimpleIoc.Default.Register<IBluetoothConnector, BluetoothConnector>();
                SimpleIoc.Default.Register<MainPageVm>();
                SimpleIoc.Default.Register<WallsPageVm>();
                SimpleIoc.Default.Register<ConfigSelectPageVm>();
                SimpleIoc.Default.Register<HistoryPageVm>();
        }

        public MainPageVm Main
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MainPageVm>();
            }
        }

        public WallsPageVm WallsVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<WallsPageVm>();
            }
        }

        public ConfigSelectPageVm ConfSelectPageVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<ConfigSelectPageVm>();
            }
        }

        public HistoryPageVm HistoryVm
        {
            get
            {
                return SimpleIoc.Default.GetInstance<HistoryPageVm>();
            }
        }


    }
}
