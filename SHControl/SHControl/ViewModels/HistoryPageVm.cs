﻿using GalaSoft.MvvmLight.Command;
using SHControl.Models;
using SHControl.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Linq;

namespace SHControl.ViewModels
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public class HistoryPageVm : BaseViewModel
    { 
        //команды появления и исчезновения страницы
        public ICommand AppearingCommand { get; set; }
        public ICommand DisappearingCommand { get; set; }        

        /// <summary>
        /// Команда сортировки истории по времени
        /// </summary>
        public ICommand TimeSortCommand { get; set; }
        /// <summary>
        /// Команда сортировки истории по действию (признаку)
        /// </summary>
        public ICommand ActionSortCommand { get; set; }

        
        private List<HistoryData> _historyData;
        /// <summary>
        /// Данные истории
        /// </summary>
        public List<HistoryData> HData
        {
            get { return _historyData; }
            set
            {
                SetProperty(ref _historyData, value);
            }
        }

        /// <summary>
        /// По исчезновению окна очистить данные истории
        /// </summary>
        public void OnDisappearing()
        {
            HData.Clear();
            HData = null;
        }

        //флаги сортировки по возрастанию или убыванию для сортировки по времени и признаку
        private bool IsDownSortTime { get; set; }
        private bool IsDownSortMarker { get; set; }

        private IUserDataStore _dataStore;
        public HistoryPageVm(IUserDataStore dataStore)
        {
            _dataStore = dataStore; 
            AppearingCommand = new RelayCommand(OnAppearingCommand);
            DisappearingCommand = new RelayCommand(OnDisappearingCommand);
            TimeSortCommand = new RelayCommand(OnTimeSortCommand);
            ActionSortCommand = new RelayCommand(OnActionSortCommand);
            IsDownSortTime = true;
            IsDownSortMarker = true;
            Title = "История";
        }


        private void OnDisappearingCommand()
        {
            
        }

        /// <summary>
        /// При появлении окна инициализировать данные истории
        /// </summary>
        private void OnAppearingCommand()
        {
            HData = new List<HistoryData>(_dataStore.GetHistoryData());
        }

        /// <summary>
        /// Сортировка по действию
        /// </summary>
        private void OnActionSortCommand()
        {
            if (HData.Count > 2)
            {
                if (IsDownSortMarker)
                {
                    HData = new List<HistoryData>(HData.OrderByDescending(v => v.Marker));
                }
                else
                {
                    HData = new List<HistoryData>(HData.OrderBy(v => v.Marker));
                }
                IsDownSortMarker = !IsDownSortMarker;
            }
        }


        /// <summary>
        /// Сортировка по времени
        /// </summary>
        private void OnTimeSortCommand()
        {
            if (HData.Count > 2)
            {
                if (IsDownSortTime)
                {
                    HData = new List<HistoryData>(HData.OrderByDescending(v => v.Date));
                }
                else
                {
                    HData = new List<HistoryData>(HData.OrderBy(v => v.Date));
                }
                IsDownSortTime = !IsDownSortTime;
            }
        }
    }
}
