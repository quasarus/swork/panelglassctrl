﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConfigSelectPage : ContentPage
	{
		public ConfigSelectPage ()
		{
			InitializeComponent ();
            BindingContext = App.Locator.ConfSelectPageVm;
        }

        /// <summary>
        /// При исчезновении страницы
        /// </summary>
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.Locator.ConfSelectPageVm.OnDisapearing();
            Content = null;                                 //очистка контента
            BindingContext = null;                          //очистка контекста
                        
        }

        protected override void OnAppearing()
        {            
            base.OnAppearing();
        }

        

        protected override bool OnBackButtonPressed()
        {
            GoBack();
            return true;
        }

        private async void GoBack()
        {

            //await App.Current.MainPage.Navigation.PopModalAsync();
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new WallsPage()), false);
        }
    }
}