﻿using GalaSoft.MvvmLight.Messaging;
using SHControl.Messages;
using SHControl.Models;
using SHControl.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        public HistoryPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.HistoryVm;            
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.Locator.HistoryVm.OnDisappearing();
            Content = null;                             //очистка контента 
            BindingContext = null;                      //очистка контекста

        }

        /// <summary>
        /// По нажатию кнопки назад на странице
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackButtonPressed()
        {            
            GoBack();
            return true;
        }

        /// <summary>
        /// Возврат на страницу команд
        /// </summary>
        private async void GoBack()
        {
            await App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(new ConfigSelectPage()), false);
        }
    }
}