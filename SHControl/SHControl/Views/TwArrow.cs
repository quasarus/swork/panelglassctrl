﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    /// <summary>
    /// Рисование стрелки для элементов команд с подкомандами
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class TwArrow : SKCanvasView
    {

        public static readonly BindableProperty IsExpandedProperty = BindableProperty.Create(nameof(IsExpanded),
            typeof(bool), typeof(TwArrow), false,
            propertyChanging: (currentControl, oldValue, newValue) =>
            {
                var thisControl = currentControl as TwArrow;
                thisControl.IsExpanded = (bool)newValue;
            });


        

        private bool _isExpanded;
        /// <summary>
        /// Флаг разворачивания подкоманд
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                
                InvalidateSurface();
                
                
            }
        }
                

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);
            
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();            
            var pathStroke2 = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.StrokeAndFill,
                Color = SKColors.Black,
                StrokeWidth = 1
            };
            var path = new SKPath { FillType = SKPathFillType.EvenOdd };
            if (!IsExpanded)
            {
                path.MoveTo(0.3f * info.Height, 0.3f * info.Height);
                path.LineTo(0.7f * info.Height, 0.5f * info.Height);
                path.LineTo(0.3f * info.Height, 0.7f * info.Height);
                path.LineTo(0.3f * info.Height, 0.3f * info.Height);
            }
            else
            {
                path.MoveTo(0.3f * info.Height, 0.7f * info.Height);
                path.LineTo(0.7f * info.Height, 0.7f * info.Height);
                path.LineTo(0.7f * info.Height, 0.3f * info.Height);
                path.LineTo(0.3f * info.Height, 0.7f * info.Height);
            }            
            path.Close();            
            canvas.DrawPath(path, pathStroke2);
        }



    }
}
