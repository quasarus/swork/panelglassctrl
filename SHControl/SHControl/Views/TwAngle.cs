﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    /// <summary>
    /// Рисование уголка для элементов команд с отступом
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class TwAngle : SKCanvasView
    {
        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();
            var pathStroke = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Stroke,
                Color = SKColors.Black,
                StrokeWidth = 3
            };
            var path = new SKPath { FillType = SKPathFillType.EvenOdd };

            path.MoveTo(0.5f * info.Height, 0);
            path.LineTo(0.5f * info.Height, 0.5f * info.Height);
            path.LineTo(info.Height, 0.5f * info.Height);

            canvas.DrawPath(path, pathStroke);
        }
    }

}
