﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    /// <summary>
    /// Сетка, по которой можно выполнять тап пальцем
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class TapedGrid: Grid,IDisposable
    {       
        public static readonly BindableProperty TapCommandProperty =
        BindableProperty.Create(nameof(TapCommand), typeof(ICommand), typeof(TapedGrid), null);
        /// <summary>
        /// Команда тапа
        /// </summary>
        public ICommand TapCommand
        {
            get { return (ICommand)GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }        

        /// <summary>
        /// Флаг включения возможности тапа
        /// </summary>
        public static readonly BindableProperty IsTapEnabledProperty =
        BindableProperty.Create(nameof(IsTapEnabled), typeof(bool), typeof(TapedGrid), false,
            propertyChanging: (currentControl, oldValue, newValue) =>
        {
            var thisControl = currentControl as TapedGrid;
            thisControl.IsTapEnabled = (bool)newValue;
        });

        public bool IsTapEnabled
        {
            get { return (bool)GetValue(IsTapEnabledProperty); }
            set
            {                
                SetValue(IsTapEnabledProperty, value);
            }
        }

        /// <summary>
        /// Флаг изменения состояния при тапе
        /// </summary>
        public static readonly BindableProperty IsSetStateProperty =
        BindableProperty.Create(nameof(IsSetState), typeof(bool), typeof(TapedGrid), false,
            propertyChanging: (currentControl, oldValue, newValue) =>
            {
                var thisControl = currentControl as TapedGrid;
                thisControl.IsSetState = (bool)newValue;
            });

        public bool IsSetState
        {
            get { return (bool)GetValue(IsSetStateProperty); }
            set
            {
                SetValue(IsSetStateProperty, value);
            }
        }


        public TapedGrid()
        {
            TapGestureRecognizer gridTap = new TapGestureRecognizer();
            gridTap.Tapped += async (sender, e) => {
                if (!IsTapEnabled)
                {
                    return;
                }
                else
                {
                    IsSetState = !IsSetState;                       //поменять флаг состояния при тапе
                }
                if (TapCommand != null)                             //выполнить команду тапа
                {
                    if (TapCommand.CanExecute(null))
                    {
                        TapCommand.Execute(null);
                    }
                }

                BackgroundColor = Color.OrangeRed;                  //окрасить сетку при тапе
                await this.ScaleTo(1.1, 100, Easing.SinInOut);      //увеличить масштаб содержимого
                await this.ScaleTo(1, 100, Easing.SinInOut);        //вернуть обратно масштаб содержимого
                BackgroundColor = Color.Transparent;                //установить цвет в прозрачный
                
            };
            this.GestureRecognizers.Add(gridTap);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TapedGrid() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
