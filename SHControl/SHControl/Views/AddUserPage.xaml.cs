﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddUserPage : ContentPage
	{
		public AddUserPage ()
		{
			InitializeComponent ();
            BindingContext = App.Locator.Main;
		}

        /// <summary>
        /// При скрытии страницы
        /// </summary>
        protected override void OnDisappearing()
        {            
            base.OnDisappearing();
            Content = null;                     //обнулить содержимое
            BindingContext = null;              //обнулить контекст привязки
        }
        
    }
}