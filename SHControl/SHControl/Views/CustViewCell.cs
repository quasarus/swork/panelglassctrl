﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.Views
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public class CustViewCell : ViewCell
    {
        public CustViewCell()
        {
            this.Disappearing += CustViewCell_Disappearing; 
        }

        /// <summary>
        /// Событие исчезновения ячейки ListView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustViewCell_Disappearing(object sender, EventArgs e)
        {
            this.Disappearing -= CustViewCell_Disappearing;
            sender = null;            
        }

        protected override void OnBindingContextChanged()
        {            
            base.OnBindingContextChanged();           
        }

    }
}
