﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.Views
{
    [Android.Runtime.Preserve(AllMembers = true)]
    public class CustListView : ListView
    {

        public CustListView()
        {
            this.ItemSelected += CustListView_ItemSelected;
        }

        /// <summary>
        /// По событию выбора элементов ListView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //очистка выбранного элемента
            var list = (CustListView)sender;
            list.SelectedItem = null;
        }
    }
}
