﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.Views
{
    /// <summary>
    /// Индикатор Rssi
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class RssiIndicator : SKCanvasView
    {
        public static readonly BindableProperty RssiProperty = BindableProperty.Create(nameof(Rssi),
            typeof(int), typeof(RssiIndicator), 0,
            propertyChanging: (currentControl, oldValue, newValue) =>
            {
                if ((int)oldValue != (int)newValue)
                {
                    var thisControl = currentControl as RssiIndicator;                    
                    thisControl.Rssi = (int)newValue;
                }
                
            });        

        /// <summary>
        /// Значение Rssi
        /// </summary>
        public int Rssi
        {
            get { return (int)GetValue(RssiProperty); }
            set
            {
                SetValue(RssiProperty, value);
                InvalidateSurface();
            }
        }

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();
                        
            //рисование линий с числом, в зависимости от уровня RSSI
            SKColor color;
            float strWidth = e.Info.Width / 20;
            float horStep = ((e.Info.Width - (7 * strWidth)) / 6);
            if (Rssi > -30)
            {
                SKColor.TryParse("0b730f", out color);
                CreateLine(6, horStep, strWidth, color, info.Width, info.Height, canvas);

            }
            else if (Rssi <= -30 && Rssi > -67)
            {
                SKColor.TryParse("17991b", out color);
                CreateLine(5, horStep, strWidth, color, info.Width, info.Height, canvas);

            }
            else if (Rssi <= -67 && Rssi > -70)
            {
                SKColor.TryParse("7aff7f", out color);
                CreateLine(4, horStep, strWidth, color, info.Width, info.Height, canvas);
            }
            else if (Rssi <= -70 && Rssi > -80)
            {
                SKColor.TryParse("ffcc00", out color);
                CreateLine(3, horStep, strWidth, color, info.Width, info.Height, canvas);
            }
            else if (Rssi <= -80 && Rssi > -90)
            {
                SKColor.TryParse("ff6600", out color);
                CreateLine(2, horStep, strWidth, color, info.Width, info.Height, canvas);

            }
            else if (Rssi <= -90)
            {
                SKColor.TryParse("cc0000", out color);
                CreateLine(1, horStep, strWidth, color, info.Width, info.Height, canvas);
            }
            
        }

        /// <summary>
        /// Рисование линии с заданным цветом
        /// </summary>
        /// <param name="lineNum"></param>
        /// <param name="horisontalStep"></param>
        /// <param name="strWidth"></param>
        /// <param name="color"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="canvas"></param>
        private void CreateLine(int lineNum, float horisontalStep, float strWidth, SKColor color,int width,int height, SKCanvas canvas)
        {            
            var pathStroke = new SKPaint
            {
                IsAntialias = true,
                Style = SKPaintStyle.Stroke,
                Color = color,
                StrokeWidth = strWidth
            };
            
            float horisontalOffset = 0;

            for (int cnt = 0; cnt < lineNum; cnt++)
            {
                var path = new SKPath { FillType = SKPathFillType.EvenOdd };
                path.MoveTo((horisontalOffset + strWidth / 2), 0);
                path.LineTo((horisontalOffset + strWidth / 2), height);
                horisontalOffset += horisontalStep;
                canvas.DrawPath(path, pathStroke);
            } 


        }
    }
}
