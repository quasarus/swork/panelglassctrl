﻿using SHControl.DependencyServices;
using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {   


        public MenuPage()
        {
            InitializeComponent();

            BindingContext = App.Locator.Main;          
            
        }
    }
}