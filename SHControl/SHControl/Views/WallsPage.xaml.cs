﻿using SHControl.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WallsPage : ContentPage
	{
		public WallsPage ()
		{
			InitializeComponent ();
            BindingContext = App.Locator.WallsVm;
        }


        protected override bool OnBackButtonPressed()
        {           
            App.Locator.WallsVm.OnBackButton();            
            return true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.Locator.WallsVm.OnDisappearing();
            Content = null;
            BindingContext = null;
                        
        }

        protected override void OnAppearing()
        {            
            base.OnAppearing();
        }

    }
}