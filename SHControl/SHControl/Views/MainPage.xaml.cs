﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Linq;
using SHControl.DependencyServices;

namespace SHControl.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        
        public MainPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.Main;            
        }

        protected override void OnDisappearing()
        {
            App.Locator.Main.OnDisappearing();
            base.OnDisappearing();            
        }

        protected override bool OnBackButtonPressed()
        {
            //если в модальном стеке страница с списком стен, то выйти из приложения
            if (App.Current.MainPage.Navigation.ModalStack.Count > 0)
            {
                var wallsPage = App.Current.MainPage.Navigation.ModalStack.ElementAt(0);
                if (wallsPage != null)
                {
                    DependencyService.Get<IDepServices>().ExitApp();
                }
            }           
            
            return base.OnBackButtonPressed();
        }

        
    }
}