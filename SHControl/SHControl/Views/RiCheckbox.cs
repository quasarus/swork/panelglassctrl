﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SHControl.Views
{
    /// <summary>
    /// Чекбокс, рисуемый с помощью SkiaSharp
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [Android.Runtime.Preserve(AllMembers = true)]
    public class RiCheckbox : SKCanvasView
    {
        /// <summary>
        /// Привязываемое свойство, отображающее check/uncheck чекбокса
        /// </summary>
        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(nameof(IsSelected),
            typeof(bool), typeof(RiCheckbox), false,
            propertyChanging: (currentControl, oldValue, newValue) =>
            {
                var thisControl = currentControl as RiCheckbox;
                thisControl.IsSelected = (bool)newValue;
            });

        /// <summary>
        /// Флаг check/uncheck чекбокса
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
                InvalidateSurface();
            }
        }

        public RiCheckbox()
        {
            TapGestureRecognizer gridTap = new TapGestureRecognizer();
            gridTap.Tapped += GridTap_Tapped;            
            GestureRecognizers.Add(gridTap);
        }

        private void GridTap_Tapped(object sender, EventArgs e)
        {
            IsSelected = !IsSelected;
        }

        /// <summary>
        /// Рисование чекбокса
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaintSurface(SKPaintSurfaceEventArgs e)
        {
            base.OnPaintSurface(e);

            SKImageInfo info = e.Info;              //информация о контейнере, в котором рисуется чекбокс
            SKSurface surface = e.Surface;          //поверхность рисования
            SKCanvas canvas = surface.Canvas;       //канва рисования

            canvas.Clear();
            if (!this.IsEnabled)                    //если рисование чекбокса включено
            {
                return;
            }
            
            var pathSqStroke = new SKPaint          //начертание линии рисования
            {
                IsAntialias = true,
                Style = SKPaintStyle.Stroke,
                Color = SKColors.Black,
                StrokeWidth = 5
            };

            //путь рисования квадрата чекбокса
            var pathSq = new SKPath { FillType = SKPathFillType.EvenOdd };
            float sqWidth = 0.8f * info.Height;
            float sqHeight = 0.8f * info.Height;
            pathSq.MoveTo(sqWidth, sqHeight);
            pathSq.LineTo(sqWidth, 0.2f * info.Height);
            pathSq.LineTo(0.2f * info.Height, 0.2f * info.Height);
            pathSq.LineTo(0.2f * info.Height, sqHeight);
            pathSq.LineTo(sqWidth, sqHeight);
            
            //если чекбокс выбран - дорисовать галку чекбокса
            if (IsSelected)
            {
                var pathGStroke = new SKPaint
                {
                    IsAntialias = true,
                    Style = SKPaintStyle.Stroke,
                    Color = SKColors.DarkRed,
                    StrokeWidth = 10
                };
                var pathG = new SKPath { FillType = SKPathFillType.EvenOdd };
                
                pathG.MoveTo(0.2f * sqWidth, 0.5f * sqHeight);
                pathG.LineTo(0.5f * sqWidth, 0.8f * sqHeight);
                pathG.LineTo(0.8f * sqWidth, 0.2f * sqHeight);
                
                canvas.DrawPath(pathG, pathGStroke);
            }

            //закрыть линию, нарисовать линию
            pathSq.Close();
            canvas.DrawPath(pathSq, pathSqStroke);
        }
        
    }
}
