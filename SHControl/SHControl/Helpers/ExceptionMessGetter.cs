﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHControl.Helpers
{
    /// <summary>
    /// Вспомогательный класс обработки исключений
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public static class ExceptionMessGetter
    {
        /// <summary>
        /// Получение списка внутренних исключений
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static IEnumerable<Exception> GetInnerExceptions(Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            var innerException = ex;
            do
            {
                yield return innerException;
                innerException = innerException.InnerException;
            }
            while (innerException != null);
        }

        /// <summary>
        /// Получение сообщений всех внутренних исключений
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetExMessages(Exception ex)
        {
            IEnumerable<Exception> excs = GetInnerExceptions(ex);
            string mess = "";
            foreach (var exception in excs)
            {
                mess += exception.Message + "\r\n";
            }
            return mess.TrimEnd(new char[] { '\r', '\n' });
        }
    }
}
