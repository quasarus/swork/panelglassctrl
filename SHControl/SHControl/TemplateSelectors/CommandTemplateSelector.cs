﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.TemplateSelectors
{
    /// <summary>
    /// Селектор шаблонов для BindableLayout
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class CommandTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Шаблон элемента простой команды
        /// </summary>
        public DataTemplate SimpleCommandTemplate { get; set; }
        /// <summary>
        /// Шаблон элемента команды с вложенными командами
        /// </summary>
        public DataTemplate SimpleCommandTemplateSub { get; set; }
        /// <summary>
        /// Шаблон элемента команды с слайдером
        /// </summary>
        public DataTemplate SimpleCommandTemplateSlider { get; set; }
        /// <summary>
        /// Шаблон элемента команды с Entry
        /// </summary>
        public DataTemplate SimpleCommandTemplateTextbox { get; set; }
        /// <summary>
        /// Шаблон элемента команды с пикером
        /// </summary>
        public DataTemplate SimpleCommandTemplatePicker { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //в зависимости от типа команды выбрать шаблон
            if (item == null)
            {
                return SimpleCommandTemplate;
            }
            try
            {
                if (item is SimpleCommandSub)
                {
                    return SimpleCommandTemplateSub;
                }
                else if (item is SimpleCommandSlider)
                {
                    return SimpleCommandTemplateSlider;
                }
                else if (item is SimpleCommandTextbox)
                {
                    return SimpleCommandTemplateTextbox;
                }
                else if (item is SimpleCommandPicker)
                {
                    return SimpleCommandTemplatePicker;
                }
                else if (item is SetTimeCommand)
                {
                    return SimpleCommandTemplate;
                }
                else if (item is SetDriverTypeCommand)
                {
                    return SimpleCommandTemplatePicker;
                }
                else if (item is SetPanelTypeCommand)
                {
                    return SimpleCommandTemplatePicker;
                }
                else if (item is SetPanelWallNameCommand)
                {
                    return SimpleCommandTemplateTextbox;
                }
                else if (item is SetPanelNameCommand)
                {
                    return SimpleCommandTemplateTextbox;
                }
                else if (item is GetHistoryCommand)
                {
                    return SimpleCommandTemplateTextbox;
                }
                return SimpleCommandTemplate;
            }
            catch (Exception)
            {
                return SimpleCommandTemplate;
            }


            
        }
    }
}
