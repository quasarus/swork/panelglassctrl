﻿using SHControl.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SHControl.TemplateSelectors
{
    /// <summary>
    /// Селектор шаблонов для ListView
    /// </summary>
    [Android.Runtime.Preserve(AllMembers = true)]
    public class CommandTemplateSelectorList : DataTemplateSelector
    {
        /// <summary>
        /// Шаблон элемента простой команды
        /// </summary>
        public DataTemplate SimpleCommandTemplateList { get; set; }
        /// <summary>
        /// Шаблон элемента команды с вложенными командами
        /// </summary>
        public DataTemplate SimpleCommandTemplateSubList { get; set; }
        /// <summary>
        /// Шаблон элемента команды с слайдером
        /// </summary>
        public DataTemplate SimpleCommandTemplateSliderList { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //в зависимости от типа команды выбрать шаблон
            try
            {
                if (item is SimpleCommandSub)
                {
                    return SimpleCommandTemplateSubList;
                }
                else if(item is SimpleCommandSlider)
                {
                    return SimpleCommandTemplateSliderList;
                }
                else if(item is GetVersionCommand)
                {
                    return SimpleCommandTemplateList;
                }
                return SimpleCommandTemplateList;
            }
            catch (Exception)
            {
                return SimpleCommandTemplateList;
            }



        }
    }
}
